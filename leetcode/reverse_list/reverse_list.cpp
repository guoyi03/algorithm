/*
 * Author     : Guo Yi
 * Create Time: 2013.10.28
 * Problem    : Reverse Linked List II 
 *              Reverse a linked list from position m to n. Do it in-place and in one-pass.
 *              (1 ≤ m ≤ n ≤ length of list.)
 * Solution   : just traversal
 *
 */


#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
ListNode *reverseBetween(ListNode *head, int m, int n) {
    ListNode * last = NULL;
    ListNode * tmp;
    ListNode * start0;
    ListNode * start1;
    ListNode * iter = head;
    if(head==NULL)return NULL;

    int num =1;
    while(iter)
    {
        if(num==m)
        {
            start0 = last;
            start1 = iter;
            last = iter;
            iter = iter->next;
        }
        else
        {
            if(num>m && num <n)
            {
                tmp = iter->next;
                iter->next = last;
                last = iter;
                iter = tmp;
            }
            else
            {
                if(num == n) 
                {
                    tmp = iter->next;
                    iter->next = last;
                    last = iter;
                    start1->next = tmp;
                    if(start0!=NULL)
                        start0->next = last;
                    else
                        head = last;
                    iter = tmp;
                }
                else
                {
                    last = iter;
                    iter = iter->next;
                }
            }
        }
        num++;
    }
    return head;
}
int main()
{
    ListNode * l1 = new ListNode(1);
    ListNode * l2 = new ListNode(2);
    ListNode * l3 = new ListNode(3);
    ListNode * l4 = new ListNode(4);
    ListNode * l5 = new ListNode(5);
    l1->next = l2;
    //l2->next = l3;
    //l3->next = l4;
    //l4->next = l5;

    l1 =reverseBetween(l1,1,2);
    while(l1)
    {
        cout<<l1->val<<" ";
        l1 = l1->next;
    }
    cout<< endl;

    delete l1,l2,l3,l4,l5;
    return 0;
}




