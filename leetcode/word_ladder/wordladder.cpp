/*
 * Author     : Guo Yi
 * Create Time: 2013-10-10
 * Problem    : Word Ladder
 *              Given two words (start and end), and a dictionary, 
 *              find the length of shortest transformation sequence from start to end
 *              http://oj.leetcode.com/problems/word-ladder/
 * Solution   : g++ wordladder.cpp  -std=c++0x
 *              method 1: Floyd algorithm O(n^3) //Time Limit Exceeded
 *
 */


#include<iostream>
#include<string>
#include<vector>
#include<unordered_set>
using namespace std;

#define MAX 10000000
vector < vector<int> > edge;

int compareString(string v1, string v2)
{
    int flag = 0;
    for(int i = 0; i< v1.size(); i++)
    {
        if(v1[i]!=v2[i])
            flag++;
        if(flag>1)return MAX;
    }
    if(flag ==0) return 0;
    return 1; // one change
}
int createGraph(unordered_set<string> &dict)
{
    int size = dict.size();

    int i = 0;
    int j = 0;

    for (auto itr = dict.begin() ; itr != dict.end() ; ++ itr)
    {
        vector<int> tmp; 
        for (auto itrk = dict.begin() ; itrk != dict.end() ; ++ itrk)
        {
            if(*itr == *itrk)
                tmp.push_back(0);
            else
                tmp.push_back(compareString(*itr, *itrk)); //edge[i][i] =1;
        }
        edge.push_back(tmp);
//        cout << *itr  <<endl;
    }
}
int calPath( )
{
    int i , j ,k ;
    int n = edge.size();

    for(k=0;k<n;k++)
    {
        for(i=0;i<n;i++)
            for(j=0;j<n;j++)
            {
                if(edge[i][j]>(edge[i][k]+edge[k][j]))
                {
                    edge[i][j] = edge[i][k]+edge[k][j] ;
                }
            }
    }
    
    for(i=0; i <n; i++)
    {
        for(j=0; j <n; j++)
        {
            cout<< edge[i][j]<<" ";
        }
        cout<<endl;
    }


}
int ladderLength(string start, string end, unordered_set<string> &dict) {
    int i = 0,j = 0;
    int tmp1 =0 ;
    int tmp2 = 0;
    int minstep = MAX;
    int first =  0;
    int second = 0;
    if(dict.empty())return 0;
    
    createGraph(dict);

    calPath();
    for (auto itr = dict.begin() ; itr != dict.end() ; ++ itr)
    {
        first = 0;
        tmp1 = compareString(start,*itr);
        if(tmp1<=1)
        {
            if(tmp1==0)
                first = 0;
            if(tmp1==1)
                first = 1;

            j = 0;
            for (auto itrk = dict.begin() ; itrk != dict.end() ; ++ itrk)
            {
                second = 0;
                tmp2 = compareString(end, *itrk) ;
                if(tmp2 <= 1)
                {
                    if(tmp2==0)
                        second = 0;
                    if(tmp2==1)
                        second = 1;
                    if(minstep > first+ second + edge[i][j])
                        minstep = first + second + edge[i][j];
                }
                j++;
            }
            if(first == 0) break;
        }
        i++;
    }
    if(minstep ==MAX) return 0;
    return minstep+1;
}
int main()
{

    unordered_set<string> dict;
    dict.insert("hot");//= ["hot","dot","dog","lot","log"];
    dict.insert("dog");
    //dict.insert("dog");
    //dict.insert("lot");
    //dict.insert("log");
    cout << ladderLength("hot","dog",dict)<<endl;
    return 0;
}




