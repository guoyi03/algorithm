/*
 * Author     : Guo Yi
 * Create Time: 2013-10-10
 * Problem    : Word Ladder
 *              Given two words (start and end), and a dictionary, 
 *              find the length of shortest transformation sequence from start to end
 *              http://oj.leetcode.com/problems/word-ladder/
 * Solution   : g++ wordladder.cpp  -std=c++0x
 *              method 2: BFS : Accepted O(|V|+|E|)
 *
 */


#include<iostream>
#include<string>
#include<queue>
#include<unordered_set>
#include<utility>
using namespace std;

int ladderLength(string start, string end, unordered_set<string> &dict) {
    int i = 0,j = 0;
    char ch;
   
    unordered_set<string> laset = dict;

    if(dict.empty())return 0;
    
    int size = start.size();

    
    queue< pair<string,int> > searchq;

    auto gitr = laset.find(start);
    if(gitr != laset.end()) //in unordered_set
    {
        searchq.push(make_pair(start,1));
        laset.erase(gitr);
    }
    else
        searchq.push(make_pair(start,1));
    
    while( !searchq.empty())
    {
        pair<string,int> pt = searchq.front();
        searchq.pop();
        string stmp = pt.first;
        for(i = 0; i< size;i++)
        {
            char old = stmp[i];
            for(ch='a' ; ch<='z' ; ch++)
            {
                if(ch!=old)
                {
                    string tmp = stmp;
                    tmp[i] = ch;   
                    if(tmp == end)
                        return pt.second+1;
                    auto itr = laset.find(tmp);
                    if(itr != laset.end()) //in set;
                    {
                        searchq.push(make_pair(tmp,pt.second+1));
                        laset.erase(itr);
                    }
                }
            }
        }
    }
    return 0;
}
int main()
{

    unordered_set<string> dict;
    dict.insert("hot");//= ["hot","dot","dog","lot","log"];
    dict.insert("dot");
    dict.insert("dog");
    dict.insert("lot");
    dict.insert("log");
    cout << ladderLength("hit","cog",dict)<<endl;

    dict.clear();

    dict.insert("a");
    dict.insert("b");
    dict.insert("c");
    cout << ladderLength("a","c",dict)<<endl;
    return 0;
}




