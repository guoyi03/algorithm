/*
 * Author     : Guo Yi
 * Create Time: 2013-10-11
 * Problem    : Word Ladder II
 *              Given two words (start and end), and a dictionary, 
 *              find the length of shortest transformation sequence from start to end
 *              return all shortest path
 *              http://oj.leetcode.com/problems/word-ladder-ii/
 * Solution   : g++ wordladder2bfs.cpp  -std=c++0x
 *              method 2: BFS : Time Limit Exceeded  
 *                      1) use a map to store all path: Time Limit Exceeded
 *              method 3: BFS+DFS: BFS save prev words; DFS rebuild pathes;
 *                              Time Limit Exceeded 
 *              (Solution: BFS should not use stack, it should use two vector to save current level words and next level words,decrease)
 *
 */


#include<iostream>
#include<string>
#include<queue>
#include<unordered_set>
#include<unordered_map>
#include<utility>
#include<stack>
#include<algorithm>
using namespace std;


vector< vector<string> > pathes;
void buildPath(unordered_map<string, vector<string> > &traces, 
                vector<string> &path, const string &word) {
            if (traces[word].size() == 0) {
                path.push_back(word);
                vector<string> curPath = path;
                reverse(curPath.begin(), curPath.end());
                pathes.push_back(curPath);
                path.pop_back();
                return;
            }

            const vector<string> &prevs = traces[word];
            path.push_back(word);
            for (vector<string>::const_iterator citr = prevs.begin();
                    citr != prevs.end(); citr++) {
                buildPath(traces, path, *citr);
            }
            path.pop_back();
        }

int compareString(string str1, string str2)
{
    int diff = 0;
    int size = str1.size();
    for(int i=0; i<size; i++)
    {
        if(str1[i]!=str2[i]) diff ++;
    }
    return diff;
}

vector<vector<string>> findLadders(string start, string end, unordered_set<string> &dict) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
    int i = 0,j = 0;
    char ch;
    
    int curlevel;
    int reslevel = 1000000;

    int flag  = 0;
    string stmp;
    string tmp;
    unordered_set<string> laset = dict;
    
    //cout<< reslevel << endl;
    vector<string> initpath;
    
    vector<string> vt;
    
    unordered_map<string, vector<string> > pathmap;
    unordered_map<string, int  > levelmap;
    
    if(dict.empty())return pathes;
    
    int size = start.size();
    
    queue< string > searchq;
    
    pathmap[start] = initpath;
    levelmap[start] = 1;
    
    searchq.push(start);
    auto gitr = laset.find(start);
    if(gitr != laset.end()) //in unordered_set
        laset.erase(start);
    //BFS
    while( !searchq.empty() )
    {
        stmp  = searchq.front();
        curlevel = levelmap[stmp];
        //cout << "*******[pop out [" << pt.str << "]] "<< pt.level <<" "<< pathmap[pt.str].size() <<endl;
        searchq.pop();
        //if(flag==1)
        {
            int diff = compareString(stmp,end);
            if(diff+curlevel>reslevel)continue;
        }

        if(levelmap[stmp] +1 >reslevel)
            break;
        
        for(i = 0; i< size;i++)
        {
            for(ch='a' ; ch<='z' ; ch++)
            {
                if(ch!=stmp[i])
                {
                    tmp = stmp;
                    tmp[i] = ch;
                    if(flag!=1)
                    {
                        if(levelmap[tmp]!=0 && curlevel+1 > levelmap[tmp]) break;
                    //cout<<pt.str<<" " <<levelmap[pt.str] <<" " <<pt.level+1  <<endl;
                        if(dict.count(tmp))
                        {
                            pathmap[tmp].push_back(stmp); //next
                            auto itr = laset.find(tmp);
                            if(itr != laset.end()) //in set;
                            {
                                levelmap[tmp] = curlevel + 1;
                                if(curlevel+1 < reslevel)
                             //cout <<"-----map["<< tmp<< "] in to queue " << pathmap[tmp].size() <<endl;
                                    searchq.push(tmp);
                                laset.erase(itr);
                            }
                        }
                    }
                    if(tmp == end)
                    {
                        pathmap[tmp].push_back(stmp);
                        if(flag == 0)
                        {
                            reslevel = curlevel+1;
                            flag = 1;
                        }
                    }
                }
            }
        }
    }

    //DFS : create result
    vector<string> path;
    pathes.clear();
    buildPath(pathmap, path, end);
    return pathes;
}
int main()
{

    unordered_set<string> dict;

    dict.insert("hot");//= ["hot","dot","dog","lot","log"];
    dict.insert("dot");
    dict.insert("dog");
    dict.insert("lot");
    dict.insert("log");
    vector< vector<string> > tmp;
    vector<string> tmp1;

    tmp = findLadders("hit","cog",dict);
    for(int i = 0; i< tmp.size() ; i++)
    {
        tmp1 = tmp[i];
        for(int j =0; j < tmp1.size();j++)
            cout<< tmp[i][j]<< " ";
        cout<<endl;
    }
    return 0;
}




