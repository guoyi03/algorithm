/*
 * Author     : Guo Yi
 * Create Time: 2013.11.16
 * Problem    : Minimum Window Substring
 *              Given a string S and a string T,
 *              find the minimum window in S which will contain all the characters in T
 *              http://oj.leetcode.com/problems/minimum-window-substring/
 * Solution   : Greedy algorithm
 *              amazing!!!
 *              use two index to find the start and the end of the sub string which contains all the characters in T
 *              when the end index moves, if the char is in T, we can move start index forward,
 *              as we can assume that the char appears at the end index, not the start.
 *
 */


#include<iostream>
#include <string>
using namespace std;

string minWindow(string S, string T) {
    if(S.empty()||T.empty()) return "";

    int record1[260]={0};
    int record2[260]={0};
    int i =0;
    int start =0;
    int end =0;
    int csize = T.size();
    int minlen = 0x10000;
    int minstart =0;

    for(i =0; i<csize ;i++)
    {
        record1[T[i]]++;
        record2[T[i]]++;
    }

    for(end=0; end<S.size(); end++)
    {
        if(record2[S[end]] >0) // exist in T
        {
            record1[S[end]]--;  // --, 
            if(record1[S[end]] >=0) //
                csize--;
        }
        if(csize==0)
        {

            while(true)
            {
                if(record2[S[start]]>0) // exist in T
                {
                    if(record1[S[start]]<0) // this char in start position does not needed
                        record1[S[start]]++;
                    else
                        break;
                }
                start++; //next
            }
            //we find the start position and the end positon when contain the all the chars in T
            if(end-start+1<minlen)
            {
                minlen = end-start+1; // modify minlen;
                minstart = start;
            }
        }
    }
    if(minlen==0x10000) 
        return "";
    else
        return S.substr(minstart, minlen);
}

int main()
{
    string S="ADOBECODEBANC";
    string T="ABC";
    cout<<minWindow(S,T)<<endl;
    return 0;
}




