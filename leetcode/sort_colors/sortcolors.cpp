/*
 * Author     : Guo Yi
 * Create Time: 2013.11.17
 * Problem    : Given an array with n objects colored red, white or blue
 *              sort them so that objects of the same color are adjacent,
 *              with the colors in the order red, white and blue.
 *              Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
 *              http://oj.leetcode.com/problems/sort-colors/
 *
 * Solution   : Use two index pointer, search only one-pass
 *
 */


#include<iostream>
using namespace std;

void sortColors(int A[], int n) {
    if(n==0) return;

    int index0=0;   // color red pointer, means when the element index is less than index0, the color is red
    int index2=n-1; // color blue pointer, means when the element index is greater than index2, the clor is blue

    int i =0;
    int tmp =0;

    for(i = 0; i<n; i++) //i means the next
    {
        if(i>index2) break;
        if(A[i]==0)
        {
            tmp = A[index0];
            A[index0] = 0;
            index0 ++;
            A[i] = tmp;
        }
        if(A[i]==2)
        {
            tmp = A[index2];
            A[index2] = A[i];
            index2--;
            A[i] = tmp;
            i--;
        }
    }
    return;
}

int main()
{
    int A[2] = {1,0};
    sortColors(A,2);

    for(int i =0 ;i<2; i++)
        cout<< A[i] <<" ";
    cout<<endl;

    return 0;
}




