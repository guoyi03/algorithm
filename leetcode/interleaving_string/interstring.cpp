/*
 * Author     : Guo Yi
 * Create Time: 2013.10.26
 * Problem    : Interleaving String
 *              Given s1, s2, s3, find whether s3 is formed by the interleaving of s1 and s2.
 *              http://oj.leetcode.com/problems/interleaving-string/
 *
 * Solution   : DFS + Backtracking
 *              Use map to store the result that had already searched to prevent repetitive search.
 *              Otherwise it will case a result of "Time Limit Exceeded"
 *
 */


#include<iostream>
#include<string>
#include<map>
using namespace std;

map<string,int> g_findstring;

bool findString(string s1,string s2, string s3)
{
    if(s1.empty())
    {
        if(s2==s3)return true;
        else return false;
    }
    if(s3.empty())return false;
    if(s1.size()+s2.size()!=s3.size())return false;
    int found;
    string s3tmp = s3;
    string s2tmp = s2;
    string s1tmp = s1;
    found = s3tmp.find_first_of(s1[0]);
    while(found!=string::npos)
    {
        if(found>s2.size())break;
        if(s3.substr(0,found)!=s2.substr(0,found))break;
        s1.erase(s1.begin(),s1.begin()+1);
        s2.erase(s2.begin(),s2.begin()+found);
        s3.erase(s3.begin(),s3.begin()+found+1);
        if(g_findstring[s1+","+s2+","+s3]==1) break;
        if(findString(s1,s2,s3)) 
            return true;
        else
            g_findstring[s1+","+s2+","+s3]=1;

        s1 = s1tmp;
        s2 = s2tmp;
        s3 = s3tmp;
        found = s3.find_first_of(s1[0],found+1);
    }
    return false;
}

bool isInterleave(string s1, string s2, string s3) {
    // Note: The Solution object is instantiated only once and is reused by each test case.
    g_findstring.clear();
    return findString(s1,s2,s3);
}

int main()
{
    string s1="abbbbbbcabbacaacccababaabcccabcacbcaabbbacccaaaaaababbbacbb";
    string s2="ccaacabbacaccacababbbbabbcacccacccccaabaababacbbacabbbbabc";
    string s3="cacbabbacbbbabcbaacbbaccacaacaacccabababbbababcccbabcabbaccabcccacccaabbcbcaccccaaaaabaaaaababbbbacbbabacbbacabbbbabc";
    cout<<isInterleave(s1,s2,s3)<<endl;
    return 0;
}




