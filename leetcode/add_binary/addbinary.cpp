/*
 * Author     : Guo Yi
 * Create Time: Add Binary
 * Problem    : Given two binary strings, return their sum (also a binary string).
 *
 * Solution   :
 *
 */


#include<iostream>
using namespace std;

char addbin(char a,char b, int&flag)
{
    if(a=='0')
    {
        if(flag==0)
            return b;
        if(flag==1)
        {
            if(b=='0')
            {
                flag =0;
                return '1';
            }
            else
            {
                flag = 1;
                return '0';
            }
        }
    }
    if(a=='1')
    {
        if(flag==0)
        {
            if(b=='0')
                return '1';
            else
            {
                flag = 1;
                return '0';
            }
        }
        else
        {
            flag =1;
            return b;
        }
    }
}
string addBinary(string a, string b) {
    
    if(a.empty()) return b;
    if(b.empty()) return a;
    int sizea = a.size();
    int sizeb = b.size();
    int i =0;
    int flag =0;
    
    string result="";

    if(sizea > sizeb)
    {
        for(i=sizea-1; i>=0 ;i--)
        {
            if(i>=sizea-sizeb)
               result.insert(result.begin(),addbin(a[i],b[i-sizea+sizeb],flag));
            else
               result.insert(result.begin(),addbin(a[i],'0',flag));
        }
    }
    else
    {
        for(i=sizeb-1; i>=0 ;i--)
        {
            if(i>=sizeb-sizea)
               result.insert(result.begin(),addbin(a[i-sizeb+sizea],b[i],flag));
            else
               result.insert(result.begin(),addbin('0',b[i],flag));
        }
    }
    if(flag==1)
        result.insert(result.begin(),'1');
    return result;
}
int main()
{

    string a="11";
    string b="1";
    cout<< addBinary(a,b)<<endl;
    return 0;
}




