/*
 * Author     : Guo Yi
 * Create Time: 2013.10.23
 * Problem    : Given a binary tree, determine if it is height-balanced.
 *              http://oj.leetcode.com/problems/balanced-binary-tree/
 *
 * Solution   :
 *
 */


#include<iostream>
#include<algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
//>=0: balance tree (contain level: if the binary tree is a balance tree, return the depth)
//-1: not balance tree
int balanceDept(TreeNode *root)
{
    if(root==NULL) return 0;
    int left =0;
    int right =0;
    
    if(root->left == NULL)
    {
        if(root->right==NULL){
            return 0;
        }
        if(root->right->left!=NULL || root->right->right!=NULL) return -1; //
        return 1;
    }

    if(root->right ==NULL)
    {
        if(root->left->left!=NULL || root->left->right !=NULL) return -1;//
        return 1;
    }

    left = balanceDept(root->left);
    if(left<0)return left; 
    right = balanceDept(root->right);
    if(right<0)return right;
    if(abs(left-right) <= 1)return max(left,right) + 1;
    else return -1;    
}

bool isBalanced(TreeNode *root) {
    if(root==NULL) return true;
    if(balanceDept(root)>=0)
        return true;
    else
        return false;
}
int main()
{
    TreeNode *n1 = new TreeNode(1);
    TreeNode *n2 = new TreeNode(2);
    TreeNode *n3 = new TreeNode(2);
    TreeNode *n4 = new TreeNode(3);
    TreeNode *n5 = new TreeNode(3);
    TreeNode *n6 = new TreeNode(4);
    TreeNode *n7 = new TreeNode(4);
    n1->left = n2;
    n1->right = n3;
    n2->left = n4;
    n2->right = n5;
    //n4->left = n6;
    //n4->right = n7;
    cout<<isBalanced(n1)<<endl;

    delete n1,n2,n3,n4,n5,n6,n7;
    return 0;
}




