/*
 * Author     : Guo Yi
 * Create Time: 2013.10.16
 * Problem    : Given a triangle, find the minimum path sum from top to bottom.
 *              Each step you may move to adjacent numbers on the row below.
 *              http://oj.leetcode.com/problems/triangle/
 *
 * Solution   : Search from top to down
 *
 */


#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int minimumTotal(vector<vector<int> > &triangle) {

    int i =0;
    int j =0;
    int size = triangle.size();

    for(i =1 ; i < size; i++)
    {
        triangle[i][0] = triangle[i-1][0] + triangle[i][0];
        for(j =1; j<i; j ++)
        {
            triangle[i][j] = min(triangle[i-1][j-1],triangle[i-1][j]) + triangle[i][j];
        }
        triangle[i][i] = triangle[i-1][i-1] + triangle[i][i];
    }
    vector<int> end = triangle[size-1];

    return *min_element(end.begin(),end.end());
}

int main()
{
    vector< vector<int> > test;
    
    vector<int> n1;
    n1.push_back(2);
    test.push_back(n1);
    
    vector<int> n2;
    n2.push_back(3);
    n2.push_back(4);
    test.push_back(n2);
#if 0
    vector<int> n3;
    n3.push_back(6);
    n3.push_back(5);
    n3.push_back(7);
    test.push_back(n3);
    
    vector<int> n4;
    n4.push_back(4);
    n4.push_back(1);
    n4.push_back(8);
    n4.push_back(3);
    test.push_back(n4);
#endif
    cout<< minimumTotal(test) <<endl;
    return 0;
}




