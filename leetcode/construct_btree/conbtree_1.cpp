/*
 * Author     : Guo Yi
 * Create Time: 2013.10.24
 * Problem    : Construct Binary Tree from Inorder and Postorder Traversal
 *              Given inorder and postorder traversal of a tree, construct the binary tree.
 *              http://oj.leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
 *              
 * Solution   : recursive algorithm 
 *
 */


#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

//[startin,endin)

TreeNode * build(vector<int>&inorder, int startin, int endin,vector<int>&postorder ,int startpost,int endpost)
{
    int rootnum = postorder[endpost-1];
    TreeNode *root = new TreeNode(rootnum);
    if(startin==endin-1)
        return root;
    int pos1 = startin;
    while(inorder[pos1]!=postorder[endpost-1])pos1++;
    if(pos1>startin) //has left
        root->left = build(inorder,startin,pos1,postorder,startpost,startpost+pos1-startin);
    if(pos1+1<endin)//has right
        root->right = build(inorder,pos1+1,endin,postorder,endpost-1-(endin-pos1-1),endpost-1);
    return root;
}

TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder)
{
    if(inorder.size()==0) return NULL;
    return build(inorder,0,inorder.size(),postorder, 0, postorder.size());
}
int main()
{
    vector<int>in;
    in.push_back(1);
    in.push_back(2);
    in.push_back(3);
    in.push_back(4);

    vector<int>post;
    post.push_back(3);
    post.push_back(2);
    post.push_back(4);
    post.push_back(1);

    buildTree(in,post);
    return 0;
}




