/*
 * Author     : Guo Yi
 * Create Time: 2013.10.24
 * Problem    : Construct Binary Tree from Preorder and Inorder Traversal
 *              Given preorder and inorder traversal of a tree, construct the binary tree.
 *              http://oj.leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
 *              
 * Solution   : Recursive algorithm 
 *
 */


#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

//[startin,endin)

TreeNode * build(vector<int>&preorder, int startpre, int endpre,vector<int>&inorder ,int startin,int endin)
{
    int rootnum = preorder[startpre];
    TreeNode *root = new TreeNode(rootnum);
    if(startpre==endpre-1)
        return root;
    int pos1 = startin;
    while(inorder[pos1]!=preorder[startpre])pos1++;
    if(pos1>startin) //has left
        root->left = build(preorder,startpre+1,startpre+1+(pos1-startin),inorder,startin,pos1);
    if(pos1+1<endin)//has right
        root->right = build(preorder,endpre-(endin-pos1-1),endpre,inorder,pos1+1,endin);
    return root;
}

TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder)
{
    if(inorder.size()==0) return NULL;
    return build(preorder,0,preorder.size(),inorder, 0, inorder.size());
}
int main()
{
    vector<int>in;
    in.push_back(1);
    in.push_back(2);
    in.push_back(3);
    in.push_back(4);

    vector<int>post;
    post.push_back(2);
    post.push_back(1);
    post.push_back(3);
    post.push_back(4);

    buildTree(in,post);
    return 0;
}




