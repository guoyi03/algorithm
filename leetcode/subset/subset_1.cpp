/*
 * Author     : Guo Yi
 * Create Time: 2013.10.30 
 * Problem    : Subsets
 *              Given a set of distinct integers, S, return all possible subsets.
 *              http://oj.leetcode.com/problems/subsets/
 *
 * Solution   : divide and conquer
 *
 */


#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

vector<vector<int> > orderSubsets(vector<int> &order)
{
    vector<vector<int> > result;
    vector<int> tmp;
    int endnum = order[order.size()-1];
    tmp.push_back(endnum);
    if(order.size()==1){
        result.push_back(tmp);
        return result;
    }

    vector<int> ordernext(order.begin(),--order.end());
    result =orderSubsets(ordernext);
    int size = result.size();
    for(int i=0; i< size ;i++)
    {
        if(result[i].size()>=1) //
        {
            vector<int> newconm(result[i]);
            newconm.push_back(endnum);
            result.push_back(newconm);
        }
    }
    result.push_back(tmp);
    return result;

}
vector<vector<int> > subsets(vector<int> &S) {
    vector<vector<int> > result;
    vector<int> tmp;
    if(S.empty()){
        result.push_back(tmp);
        return result;
    }
    sort(S.begin(), S.end());
    result = orderSubsets(S);
    result.push_back(tmp);
    return result;
}
int main()
{
    vector< vector<int> > res;
    
    vector<int> s;
    s.push_back(1);
    s.push_back(2);
    s.push_back(3);

    res = subsets(s);
    cout<< res.size()<<endl;
    return 0;
}




