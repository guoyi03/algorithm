/*
 * Author     : Guo Yi
 * Create Time: 2013.10.30 
 * Problem    : Subsets II
 *              Given a collection of integers that might contain duplicates, S, return all possible subsets.
 *              http://oj.leetcode.com/problems/subsets-ii/
 *
 * Solution   : divide and conquer
 *
 */


#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;


vector<vector<int> > orderSubsets(vector<int> &order)
{
    int i = 0;
    int j = 0;
    vector<vector<int> > result;
    vector<int> tmp;
    int endnum = order[order.size()-1];
    int samenum = 0;
    while(order.size()-samenum-1>=0 && order[order.size()-samenum-1]==endnum)
        samenum++; //1 means unique number;
    

    if(order.size() - samenum ==0){ //
        for(i =0 ; i<samenum ; i++)
        {
            tmp.push_back(endnum);
            vector<int> tmp1(tmp);
            result.push_back(tmp1);
        }
        return result;
    }

    vector<int> ordernext(order.begin(),order.end()-samenum);
    result =orderSubsets(ordernext);
    int size = result.size();
    for(i=0; i< size ;i++)
    {
        if(result[i].size()>=1) //
        {
            vector<int> ttmp(result[i]);
            for(j =0; j<samenum; j++)
            {
                ttmp.push_back(endnum);
                vector<int> newconm(ttmp);
                result.push_back(newconm);
            }
        }
    }
    tmp.clear();
    for(i =0 ; i<samenum ; i++)
    {
        tmp.push_back(endnum);
        vector<int> tmp1(tmp);
        result.push_back(tmp1);
    }
    return result;

}
vector<vector<int> > subsetsWithDup(vector<int> &S) {
    vector<vector<int> > result;
    vector<int> tmp;
    if(S.empty()){
        result.push_back(tmp);
        return result;
    }
    sort(S.begin(), S.end());
    result = orderSubsets(S);
    result.push_back(tmp);
    return result;
}
int main()
{
    vector< vector<int> > res;
    
    vector<int> s;
    s.push_back(1);
    s.push_back(2);
    s.push_back(2);


    res = subsetsWithDup(s);
    cout<< res.size()<<endl;
    return 0;
}




