/*
 * Author     : Guo Yi
 * Create Time: 2013.11.22
 * Problem    : Plus One 
 *              Given a number represented as an array of digits
 *              plus one to the number
 *              http://oj.leetcode.com/problems/plus-one/
 * Solution   : traverse
 *
 */


#include<iostream>
#include<vector>
#include<stack>
using namespace std;

vector<int> plusOne(vector<int> &digits) {
    vector<int> result;
    stack<int> sres;
    if(digits.empty())return result;

    int size = digits.size();
    int i;
    int add = 1;
    int tmp;
    for(i = size-1 ;i>=0 ; i--)
    {
        tmp = digits[i] +add;
        if(tmp==10)
        {
            sres.push(0);
            add =1;
        }
        else
        {
            sres.push(tmp);
            add =0;
        }
    }
    if(add==1)
        sres.push(1);
    while(!sres.empty())
    {
        result.push_back(sres.top());
        sres.pop();
    }
    return result;
}
int main()
{
    vector<int> digits;
    digits.push_back(9);
    digits.push_back(9);
    digits.push_back(9);

    vector<int> res;
    res = plusOne(digits);

    for(int i=0; i<res.size(); i++)
        cout<<res[i]<<" ";
    cout<<endl;
    return 0;
}




