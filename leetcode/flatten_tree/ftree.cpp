/*
 * Author     : Guo Yi
 * Create Time: 2013.10.20
 * Problem    : Flatten Binary Tree to Linked List 
 *              Given a binary tree, flatten it to a linked list in-place.
 *              http://oj.leetcode.com/problems/flatten-binary-tree-to-linked-list/#
 *
 * Solution   : pre-order traversal
 *
 */


#include<iostream>
#include<stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void flatten(TreeNode *root) {
    if(root==NULL) return;
    TreeNode * temp = root;

    TreeNode * last ;
    
    TreeNode * tmp1 ;
    
    stack<TreeNode *> fstack;
    while(temp || !fstack.empty()) 
    {
        while(temp)
        {
            fstack.push(temp);
            temp = temp->left;
        }
        last = fstack.top();
        fstack.pop();
        temp = last->right;
        while(temp==NULL && !fstack.empty())
        {
            tmp1 = fstack.top();
            fstack.pop();
            temp = tmp1->right;
            tmp1->right = tmp1->left;
            tmp1->left = NULL;
        }
        last->right = temp;
        last->left = NULL;
    }
}
int main()
{
    struct TreeNode* t1 = new TreeNode(1);
    struct TreeNode* t2 = new TreeNode(2);
    struct TreeNode* t3 = new TreeNode(3);
    struct TreeNode* t4 = new TreeNode(4);
    struct TreeNode* t5 = new TreeNode(5);
    struct TreeNode* t6 = new TreeNode(6);

    t1->right = t2;
    t2->left = t3;
    /*t2->left = t3;
    t2->right = t4;
    t5->right = t6;*/
    flatten(t1);
    while(t1)
    {
        cout<< t1->val << " ";
        t1= t1->right;
    }
    cout<< endl;
    return 0;
}




