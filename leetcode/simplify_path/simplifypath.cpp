/*
 * Author     : Guo Yi
 * Create Time: 2013.11.23
 * Problem    : Simplify Path 
 *              Given an absolute path for a file (Unix-style), simplify it.
 *              http://oj.leetcode.com/problems/simplify-path/
 *
 * Solution   : Stack
 *
 */


#include<iostream>
#include<stack>
using namespace std;

string simplifyPath(string path) {

    if(path.empty()) return path;
    string res;
    stack<char> spath;
    int size = path.size();
    for(int i=0; i<path.size(); i++)
    {
        switch(path[i])
        {
            case '/':
                if(i!=size-1)
                    if(spath.empty()||spath.top()!='/')
                        spath.push(path[i]);
                break;
            case '.':
                if(i< size -1)
                {
                    if(spath.top()=='/'&&path[i+1]=='/') // /./
                    {
                        spath.pop();
                    }
                    else
                    {
                        if(spath.top()=='/'&&path[i+1]=='.') // /..
                        {
                            spath.pop();
                            while(!spath.empty()&&spath.top()!='/')
                                spath.pop();
                            i = i+1;
                        }
                        else
                            spath.push(path[i]);
                    }
                }
                else
                {
                    if(spath.top()='/')
                        spath.pop();
                }
                break;
            default:
                spath.push(path[i]);
                break;
        }
    }
    if(spath.empty())
        spath.push('/');
    while(!spath.empty())
    {
        res.insert(res.begin(),spath.top());
        spath.pop();
    }

    if(res.size()!=1 && res[res.size()-1]=='/')
        return res.substr(0,res.size()-1);
    return res;
}

int main()
{
    string path = "/home/";
    cout<< simplifyPath(path) <<endl;
    
    string path1 = "/a/./b/../../c/";
    cout<< simplifyPath(path1) <<endl;
    
    string path2 = "/../";
    cout<< simplifyPath(path2) <<endl;
    
    string path3 = "/home//.foo///";
    cout<< simplifyPath(path3) <<endl;

    string path4 = "/a/./b///../c/../././../d/..//../e/./f/./g/././//.//h///././/..///";
    cout<< simplifyPath(path4) <<endl;
    return 0;
}




