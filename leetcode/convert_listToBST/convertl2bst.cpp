/*
 * Author     : Guo Yi
 * Create Time: 2013.10.23
 * Problem    : Given a singly linked list where elements are sorted in ascending order
 *              convert it to a height balanced BST.
 *              http://oj.leetcode.com/problems/convert-sorted-list-to-binary-search-tree/
 *
 * Solution   : BST (recursive)
 *
 */


#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

TreeNode *sortedList(ListNode *head, int nodenum)
{
    int leftnum;
    int rightnum;
    if(nodenum==0)return NULL;
    
    leftnum = nodenum/2;
    rightnum = nodenum-leftnum-1;

    ListNode * inter = head;
    for(int i =0; i<leftnum; i++)
    {
        inter = inter->next;
    }
    TreeNode *newtnode =new  TreeNode(inter->val);

    newtnode->left = sortedList(head, leftnum);
    newtnode->right = sortedList(inter->next,rightnum);
    return newtnode;
}

//As the list has already order
TreeNode *sortedListToBST(ListNode *head) {
    ListNode * inter = head;
    int nodenum=0;
    while(inter!=NULL)
    {
        nodenum++;
        inter = inter->next;
    }
    return sortedList(head,nodenum);
}
int main()
{
    ListNode* l1 = new ListNode(1);
    ListNode* l2 = new ListNode(2);
    ListNode* l3 = new ListNode(3);
    ListNode* l4 = new ListNode(4);
    ListNode* l5 = new ListNode(5);
    //l1->next = l2;
    //l2->next = l3;
    //l3->next = l4;
    //l4->next = l5;
    TreeNode *  test =NULL;
    test = sortedListToBST(l1);
    cout<<test->val<< endl;
    
    delete l1,l2,l3,l4,l5;

    return 0;
}




