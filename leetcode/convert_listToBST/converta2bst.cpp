/* 
* Author     : Guo Yi
* Create Time: 2013.10.23
* Problem    : Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
*              http://oj.leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
*
* Solution   : BST (recursive)
*
*/

#include<iostream>
#include<vector>
using namespace std;              

struct TreeNode {          
    int val;          
    TreeNode *left;   
    TreeNode *right;  
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};                     
 
vector<int> tcopy;
TreeNode *sortedArray(int start, int nodenum)      
{                      
     int leftnum;       
     int rightnum;      
     if(nodenum==0)return NULL; 
                        
     leftnum = nodenum/2;
     rightnum = nodenum-leftnum-1;
                        
     TreeNode *newtnode =new  TreeNode(tcopy[start+ leftnum]);
     
     newtnode->left = sortedArray(start, leftnum);
     newtnode->right = sortedArray(start+leftnum+1,rightnum);
     return newtnode;   
}

//As the Array has already order
TreeNode *sortedArrayToBST(vector<int> &num) 
{
    tcopy = num;       
    return sortedArray(0,num.size());
}                      
int main()             
{                      
    vector<int> array; 
    array.push_back(1);
    array.push_back(2);
    array.push_back(3);
    array.push_back(4);
    TreeNode *  test =NULL;
    test = sortedArrayToBST(array);
    cout<<test->val<< endl;
                                                                                                                                                                                   
    return 0;          
} 
    
