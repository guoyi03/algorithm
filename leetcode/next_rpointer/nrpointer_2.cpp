/*
 * Author     : Guo Yi
 * Create Time: 2013-10-19
 * Problem    : Populating Next Right Pointers in Each Node 
 *              Given a binary tree,
 *              Populate each next pointer to point to its next right node
 *              http://oj.leetcode.com/problems/populating-next-right-pointers-in-each-node/
 *
 * Solution   :  traversal (BFS) 
 */


#include<iostream>
using namespace std;

struct TreeLinkNode {
    int val;
    TreeLinkNode *left, *right, *next;
    TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 };

TreeLinkNode* findnextpointer(TreeLinkNode *root, int flag)
{
    if(flag == 0) //start from left;
    {
        if(root->right!=NULL)
            return root->right;
    }
    while(root->next!=NULL)
    {
        if(root->next->left!=NULL)
            return root->next->left;
        if(root->next->right!=NULL)
            return root->next->right;
        root = root->next;
    }
    return NULL;
}
TreeLinkNode* connlevel(TreeLinkNode *root)
{
    if(root->left==NULL && root->right==NULL) //leaf node
        return NULL;
    if(root->left != NULL)
        root->left->next = findnextpointer(root,0);
    if(root->right!=NULL)
        root->right->next = findnextpointer(root,1);
    
    if(root->left != NULL)
        return root->left;
    if(root->right != NULL)
        return root->right;
    return NULL;
}

void connect(TreeLinkNode *root) {
    
    int flag =0;
    struct TreeLinkNode* start, *tmp1;
    struct TreeLinkNode* tstep ;
    if(root==NULL)return;
    root->next = NULL;
    if(root->left==NULL && root->right ==NULL)return;
    flag = 1;
    if(root->left!=NULL)
    {
        if(root->right!=NULL)
        {
            root->left->next= root->right;
            root->right->next = NULL;
        }
        else
            root->left->next =NULL;
        start = root->left;
    }
    else
    {
        start = root->right;
        root->right->next = NULL;
    }
    while(flag == 1) //have next level
    {
        tstep = start;
        flag = 0;
        while(tstep!=NULL) //have next
        {
            tmp1 =  connlevel(tstep);
            if(flag==0 && tmp1 !=NULL)
            {
                start = tmp1;
                flag = 1;
            }
            tstep = tstep->next;
        }
    }

}
int main()
{
    struct TreeLinkNode* t1 = new TreeLinkNode(1);
    struct TreeLinkNode* t2 = new TreeLinkNode(2);
    struct TreeLinkNode* t3 = new TreeLinkNode(3);
    struct TreeLinkNode* t4 = new TreeLinkNode(4);
    struct TreeLinkNode* t5 = new TreeLinkNode(5);
    struct TreeLinkNode* t6 = new TreeLinkNode(6);
    struct TreeLinkNode* t7 = new TreeLinkNode(7);
    struct TreeLinkNode* t8 = new TreeLinkNode(8);
    struct TreeLinkNode* t9 = new TreeLinkNode(9);
    struct TreeLinkNode* t10 = new TreeLinkNode(10);
    struct TreeLinkNode* t11 = new TreeLinkNode(11);
    struct TreeLinkNode* t12 = new TreeLinkNode(12);
    struct TreeLinkNode* t13 = new TreeLinkNode(13);
    struct TreeLinkNode* t14 = new TreeLinkNode(14);

    t1->left = t2;
    t1->right = t3;
    t2->left = t4;
    t4->left = t5;
    t4->right = t6;
    t6->left = t7;
    t6->right = t8;
    
    t7->left = t9;
    t7->right = t10;
    t8->left = t11;
    t8->right = t12;

    t12->left = t13;
    t13->right = t14;

    connect(t1);
    return 0;
}




