/*
 * Author     : Guo Yi
 * Create Time: 2013-10-19
 * Problem    : Populating Next Right Pointers in Each Node 
 *              Given a binary tree,
 *              Populate each next pointer to point to its next right node
 *              http://oj.leetcode.com/problems/populating-next-right-pointers-in-each-node/
 *
 * Solution   :  traversal (DFS)
 */


#include<iostream>
using namespace std;

struct TreeLinkNode {
    int val;
    TreeLinkNode *left, *right, *next;
    TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 };

void connlevel(TreeLinkNode *root)
{
    if(root->left==NULL ||root->right==NULL) //leaf node
        return;
    root->left->next= root->right;
    if(root->next!=NULL)
        root->right->next = root->next->left;
    else
        root->right->next = NULL;
    connlevel(root->left);
    connlevel(root->right);
    
}

void connect(TreeLinkNode *root) {
    
    if(root==NULL)return;
    root->next = NULL;
    if(root->left==NULL || root->right ==NULL)return;
    
    root->left->next= root->right;
    root->right->next = NULL;
    connlevel(root->left);
    connlevel(root->right);
}
int main()
{
    struct TreeLinkNode* t1 = new TreeLinkNode(1);
    struct TreeLinkNode* t2 = new TreeLinkNode(2);
    struct TreeLinkNode* t3 = new TreeLinkNode(3);
    struct TreeLinkNode* t4 = new TreeLinkNode(4);
    struct TreeLinkNode* t5 = new TreeLinkNode(5);
    struct TreeLinkNode* t6 = new TreeLinkNode(6);
    struct TreeLinkNode* t7 = new TreeLinkNode(7);

    t1->left = t2;
    t1->right = t3;
    t2->left = t4;
    t2->right = t5;
    t3->left = t6;
    t3->right = t7;
    connect(t1);

    return 0;
}




