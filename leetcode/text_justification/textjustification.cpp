/*
 * Author     : Guo Yi
 * Create Time: 2013.11.24
 * Problem    : Text Justification 
 *              Given an array of words and a length L
 *              format the text such that each line has exactly L characters and is fully (left and right) justified.
 *              http://oj.leetcode.com/problems/text-justification/
 * Solution   : greedy algorithm
 *
 */


#include<iostream>
#include<vector>
#include<string>
using namespace std;

vector<string> fullJustify(vector<string> &words, int L) {

    if(words.empty())return words;
    if(L==0) return words;
    int size = words.size();
    vector<string> text;
    int start =0;
    int end =0;  //[start,end]
    int jstlen =0;
    int wlen =0;
    int i=0,j=0,k=0;
    int space=0,space1=0;
    int num=0;
    jstlen = words[0].size();
    
    for(i= 1; i< size; i++)
    {
        jstlen = jstlen + words[i].size(); //
        if(jstlen > L -(end-start) -1) //
        {
            
            wlen = jstlen - words[i].size();
            num = end- start+1;
            
            string jststr="";
            if(num==1) // only contain one words
            {
                jststr = words[start];
                for(j = 0; j< L-wlen ;j++)
                    jststr = jststr + " ";
                text.push_back(jststr);
                start = i;
                end = i;
                jstlen = words[i].size();
                continue;
            }
            
            space = (L-wlen)/(num-1) ;// number of the spaces
            space1 =(L-wlen)%(num-1); // more spaces needed 

            string extra="";
            for(j=0; j<space;j++)
                extra= extra+" ";
            k = 0;
            for(j= start; j<end ;j++)
            {
                if(k<space1)
                {
                    jststr = jststr + words[j] + extra +" ";
                    k++;
                }
                else
                    jststr = jststr + words[j] + extra;
            }
            jststr = jststr +words[end];
            text.push_back(jststr);
            start = i;
            end = i;
            jstlen = words[i].size();
        }
        else
            end = i;
    }
    string jstend ="";
    for(j=start ;j <end; j++)
        jstend = jstend+ words[j]+" ";
    jstend = jstend + words[end];
    while(jstend.size()<L)
        jstend = jstend+" ";
    text.push_back(jstend);
    return text;
}
int main()
{
    vector<string> words;
    words.push_back("a");
    words.push_back("b");
    words.push_back("c");
    words.push_back("d");
    words.push_back("e");
#if 0
    words.push_back("This");
    words.push_back("is");
    words.push_back("an");
    words.push_back("example");
    words.push_back("of");
    words.push_back("text");
    words.push_back("justification.");
#endif
    vector<string> result;
    result = fullJustify(words,1);

    for(int i=0; i<result.size(); i++)
        cout<< result[i]<<endl;
    
    return 0;
}




