/*
 * Author     : Guo Yi
 * Create Time: 2013.10.25
 * Problem    : Same Tree
 *              Given a binary tree, check if they are equal or not
 *
 * Solution   : recursive
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
bool isSameTree(TreeNode *p, TreeNode *q)
{
    if (p == NULL && q == NULL)
        return true;
    if (p == NULL || q == NULL)
        return false;
    if(p->val == q->val)
    {
        return isSameTree(p->left,q->left)&&isSameTree(p->right,q->right);
    }
    return false;
}
int main()
{
    TreeNode* t1 = new TreeNode(3);
    TreeNode* t2 = new TreeNode(9);
    TreeNode* t3 = new TreeNode(3);
    TreeNode* t4 = new TreeNode(9);

    t1->left = t2;
    t3->left = t4;

    cout<< isSameTree(t1,t3)<<endl;
    
    return 0;
}




