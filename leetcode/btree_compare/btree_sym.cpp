/*
 * Author     : Guo Yi
 * Create Time: 2013.10.25
 * Problem    : Symmetric Tree
 *              Given a binary tree, check whether it is a mirror of itself
 *
 * Solution   : BFS (queue)
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
bool check(TreeNode *leftNode, TreeNode *rightNode)
{
    if (leftNode == NULL && rightNode == NULL)
        return true;
    if (leftNode == NULL || rightNode == NULL)
        return false;

    return leftNode->val == rightNode->val && check(leftNode->left, rightNode->right) && 
        check(leftNode->right, rightNode->left);
}

bool isSymmetric(TreeNode *root) {
    // Start typing your C/C++ solution below
    // DO NOT write int main() function
    if (root == NULL)
        return true;
        
    return check(root->left, root->right);
}

int main()
{
    TreeNode* t1 = new TreeNode(3);
    TreeNode* t2 = new TreeNode(9);
    TreeNode* t3 = new TreeNode(9);

    t1->left = t2;
    t1->right = t3;

    cout<< isSymmetric(t1)<<endl;
    
    return 0;
}




