/*
 * Author     : Guo Yi
 * Create Time: 2013.11.3
 * Problem    : Maximal Rectangle 
 *              Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing all ones and return its area.
 *              http://oj.leetcode.com/problems/maximal-rectangle/
 *
 * Solution   : stack
 *              (difficult, and the solution is beautiful.)
 *              use the solution from largest rect.
 *
 */


#include<iostream>
#include<vector>
#include<stack>
using namespace std;

int largestRectangleArea(vector<int> &height) {

    int maxrect = 0;
    if(height.empty())return 0;

    stack<int> sHeight;  //
    stack<int> sIndex;   //
    int tmparea = 0;

    for(int i =0 ;i< height.size(); i++)
    {
        if(sHeight.empty()||height[i]> sHeight.top())
        {
            sHeight.push(height[i]);
            sIndex.push(i);
        }
        else if(height[i] < sHeight.top())
        {
            int indexend = 0;
            while(!sHeight.empty()&& height[i]< sHeight.top())
            {
                indexend = sIndex.top();
                tmparea =  sHeight.top()*(i-indexend);
                maxrect = max(maxrect, tmparea);
                sIndex.pop();
                sHeight.pop();
            }
            sHeight.push(height[i]);
            sIndex.push(indexend);
        }
    }
    while(!sHeight.empty())
    {
        tmparea = sHeight.top()* (height.size()-sIndex.top());
        maxrect = max(maxrect,tmparea);
        sHeight.pop();
        sIndex.pop();
    }
    return maxrect;
}


int maximalRectangle(vector<vector<char> > &matrix) {
    
    if(matrix.empty()) return 0;
    
    vector< vector<int> > height ; //m*n;

    int i =0;
    int j =0;
    int maxrect =0;
    int tmprect =0;
    int xsize = matrix.size();
    int ysize = matrix[0].size();

    for(i =0; i < xsize ;i ++)
    {
        vector<int> tmp;
        for(j=0; j< ysize; j++)
            tmp.push_back(0);
        height.push_back(tmp);
    }
    
    for(j=0; j< ysize; j++)
        height[0][j] = matrix[0][j] - '0';
    for(i =1; i < xsize ;i ++)
        for(j=0; j< ysize; j++)
        {
            if(matrix[i][j]=='1')
                height[i][j] = height[i-1][j]+1;
        }

    for(i =0; i< xsize; i++)
    {
        tmprect = largestRectangleArea(height[i]);
        maxrect = max(maxrect, tmprect);
    }
    return maxrect;
}
int main()
{
    vector< vector <char> > ma;
    vector<char> tmp1;
    vector<char> tmp2;
    tmp1.push_back('1');
    tmp1.push_back('0');
    
    tmp2.push_back('1');
    tmp2.push_back('1');

    ma.push_back(tmp1);
    ma.push_back(tmp2);
    cout<<maximalRectangle(ma)<<endl;
    return 0;
}




