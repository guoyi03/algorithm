/*
 * Author     : Guo Yi
 * Create Time: 2013.11.11
 * Problem    : Largest Rectangle in Histogram  
 *              Given n non-negative integers representing the histogram's bar height where the width of each bar is 1
 *              find the area of largest rectangle in the histogram.
 *              http://oj.leetcode.com/problems/largest-rectangle-in-histogram/
 *
 * Solution   : stack
 *              (difficult, and the solution is beautiful.)
 *
 */


#include<iostream>
#include<vector>
#include<stack>

using namespace std;

int largestRectangleArea(vector<int> &height) {

    int maxrect = 0;
    if(height.empty())return 0;

    stack<int> sHeight;  //
    stack<int> sIndex;   //
    int tmparea = 0;

    for(int i =0 ;i< height.size(); i++)
    {
        if(sHeight.empty()||height[i]> sHeight.top())
        {
            sHeight.push(height[i]);
            sIndex.push(i);
        }
        else if(height[i] < sHeight.top())
        {
            int indexend = 0;
            while(!sHeight.empty()&& height[i]< sHeight.top())
            {
                indexend = sIndex.top();
                tmparea =  sHeight.top()*(i-indexend);
                maxrect = max(maxrect, tmparea);
                sIndex.pop();
                sHeight.pop();
            }
            sHeight.push(height[i]);
            sIndex.push(indexend);
        }
    }
    while(!sHeight.empty())
    {
        tmparea = sHeight.top()* (height.size()-sIndex.top());
        maxrect = max(maxrect,tmparea);
        sHeight.pop();
        sIndex.pop();
    }
    return maxrect;
}
int main()
{
    vector<int> h;
    h.push_back(3);
    h.push_back(2);
    h.push_back(3);
#if 0
    h.push_back(3);
    h.push_back(2);
    h.push_back(5);
#endif
    cout<<largestRectangleArea(h)<<endl;

    return 0;
}




