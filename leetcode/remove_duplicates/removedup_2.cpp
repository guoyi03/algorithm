/*
 * Author     : Guo Yi
 * Create Time: 2013.11.12
 * Problem    : Remove Duplicates from Sorted List II
 *              Given a sorted linked list, delete all nodes that have duplicate numbers, 
 *              leaving only distinct numbers from the original list.
 *              http://oj.leetcode.com/problems/remove-duplicates-from-sorted-list-ii/
 *
 * Solution   : search
 *
 */


#include<iostream>
#include<map>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

map<int,int> hashmap;
ListNode *deleteDuplicates(ListNode *head) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    ListNode* singlelist = NULL;
    ListNode* last = NULL;
    ListNode* tmp = head;
    hashmap.clear();
    if(head==NULL) return NULL;
    while(tmp)
    {
        hashmap[tmp->val]++;
        tmp = tmp->next;
    }
    while(head)
    {
        if(hashmap[head->val]==1)
        {
            if(singlelist==NULL)
            {
                singlelist = head;
                last = head;
            }
            else
            {
                last->next = head;
                last = head;
            }
        }
        head = head->next;
    }
    if(last!=NULL)
        last->next = NULL;
    return singlelist;
}
int main()
{
    ListNode * l1 = new ListNode(1);
    ListNode * l2 = new ListNode(1);
    ListNode * l3 = new ListNode(2);
    ListNode * l4 = new ListNode(3);
    ListNode * l5 = new ListNode(3);

    l1->next = l2;
    l2->next = l3;
    l3->next = l4;
    l4->next = l5;

    ListNode * tmp = deleteDuplicates(l1);

    while(tmp)
    {
        cout<<tmp->val<<" ";
        tmp = tmp->next;
    }
    cout<<endl;
    delete l1,l2,l3,l4,l5;
    return 0;
}




