/*
 * Author     : Guo Yi
 * Create Time: 2013.11.12
 * Problem    : Remove Duplicates from Sorted List
 *              Given a sorted linked list, delete all duplicates such that each element appear only once.
 *              http://oj.leetcode.com/problems/remove-duplicates-from-sorted-list/
 *
 * Solution   : search
 *
 */


#include<iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *deleteDuplicates(ListNode *head) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    ListNode* singlelist = head;
    ListNode* last = head;
    
    if(head==NULL) return NULL;
    head = head->next;
    while(head)
    {
        if(head->val!=last->val)
        {
            last->next = head;
            last = head;
        }
        head = head->next;
    }
    last->next =NULL;
    return singlelist;
}
int main()
{
    ListNode * l1 = new ListNode(1);
    ListNode * l2 = new ListNode(1);
    ListNode * l3 = new ListNode(2);
    ListNode * l4 = new ListNode(3);
    ListNode * l5 = new ListNode(3);

    l1->next = l2;
    l2->next = l3;
    l3->next = l4;
    l4->next = l5;

    ListNode * tmp = deleteDuplicates(l1);

    while(tmp)
    {
        cout<<tmp->val<<" ";
        tmp = tmp->next;
    }
    cout<<endl;
    delete l1,l2,l3,l4,l5;
    return 0;
}




