/*
 * Author     : Guo Yi
 * Create Time: 2013.11.15
 * Problem    : Combinations
 *              Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
 *              http://oj.leetcode.com/problems/combinations/
 * Solution   : c(n,k)=c(n-1,k-1)+c(n-1,k)
 *
 */


#include<iostream>
#include<vector>
using namespace std;

vector<vector<int> > combine(int n, int k) {
    int i =0;
    vector<vector<int> > result;
    if(n<1) return result;
    

    if(n==k)
    {
        vector<int> tmp;
        for(i=1 ;i<=n; i++)
            tmp.push_back(i);
        result.push_back(tmp);
        return result;
    }
    if(k==1)
    {
        for(i=1 ;i<=n; i++)
        {
            vector<int> tmp;
            tmp.push_back(i);
            result.push_back(tmp);
        }
        return result;
    }
    result = combine(n-1,k);
    vector<vector<int> > result2;
    result2 = combine(n-1,k-1);
    for(i =0; i< result2.size(); i++)
    {
        vector<int> tmp;
        tmp = result2[i];
        tmp.push_back(n);
        result.push_back(tmp);
    }
    return result;
}
int main()
{
    vector<vector<int> > res;
    res = combine(4,2);

    for(int i =0 ;i< res.size(); i++)
    {
        for(int j= 0; j< res[0].size(); j++)
            cout<< res[i][j]<<" ";
        cout<<endl;
    }
    return 0;
}




