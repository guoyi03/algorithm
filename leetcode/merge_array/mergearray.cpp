/*
 * Author     : Guo Yi
 * Create Time: 2013.10.31
 * Problem    : Merge Sorted Array
 *              Given two sorted integer arrays A and B, merge B into A as one sorted array.
 *              http://oj.leetcode.com/problems/merge-sorted-array/
 *
 * Solution   : search
 *
 */


#include<iostream>
using namespace std;

void merge(int A[], int m, int B[], int n) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    if(n<1) return;

    int k = m+n -1;
    int i = m-1;
    int j = n-1;

    while(i>=0 && j>=0)
    {
        if(A[i]>B[j])
        {
            A[k] = A[i];
            k--;
            i--;
        }
        else
        {
            A[k] = B[j];
            k--;
            j--;
        }
    }
    
    while(j>=0)
    {
        A[k] = B[j];
        k--;
        j--;
    }
    return ;
}
int main()
{
    int A[5]={1,2,5};
    int B[2]={3,4};

    merge(A,3,B,2);
    for(int i =0 ;i<5;i++)
        cout<< A[i]<<endl;
    return 0;
}




