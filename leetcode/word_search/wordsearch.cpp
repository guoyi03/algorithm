/*
 * Author     : Guo Yi
 * Create Time: 2013.11.15
 * Problem    : Word Search
 *              Given a 2D board and a word, find if the word exists in the grid.
 *              http://oj.leetcode.com/problems/word-search/
 *
 * Solution   : DFS + backtracking
 *
 */


#include<iostream>
#include<vector>
#include<stack>
#include<sstream>
using namespace std;


vector<vector<char> > tcopy;
vector<vector<char> > flag;
int xsize;
int ysize;

//word[0] == board[x][y]
bool findword(int x, int y, string word)
{
    flag[x][y] = 1;
    if(word.size()==1) return true; 

    string str = word.substr(1, word.size()-1);
    if(x>0)
    {
        if(flag[x-1][y]==0 && tcopy[x-1][y]==str[0]) //
        {
            if(findword(x-1, y,str))
                return true;
            else
                flag[x-1][y]=0;
        }
    }
    if(x<xsize-1)
    {
        if(flag[x+1][y]==0 && tcopy[x+1][y]==str[0]) //
        {
            if(findword(x+1, y,str))
                return true;
            else
                flag[x+1][y]=0;
        }
    }
    if(y>0)
    {
        if(flag[x][y-1]==0 &&tcopy[x][y-1]==str[0]) //
        {
            if(findword(x, y-1,str))
                return true;
            else
                flag[x][y-1]=0;
        }
    }
    if(y<ysize-1)
    {
        if(flag[x][y+1]==0 && tcopy[x][y+1]==str[0]) //
        {
            if(findword(x, y+1,str))
                return true;
            else
                flag[x][y+1]=0;
        }
    }
    return false;
}

bool exist(vector<vector<char> > &board, string word) {
    
    if(board.empty()) return false;
    if(word.empty()) return true;

    tcopy = board;
    flag= board;
    vector<vector<char> > tmp = board;
    xsize = board.size();
    ysize = board[0].size();

    int i,j;

    for(i =0; i<xsize; i++)
         for(j=0; j<ysize; j++)
         {
             flag[i][j]=0;
             tmp[i][j] =0;
         }

    for(i =0; i<xsize; i++)
        for(j=0; j<ysize; j++)
        {
            if(board[i][j]==word[0])
            {
                flag=tmp;
                if(findword(i,j,word))
                    return true;
            }
        }
    return false;

}
int main()
{
    vector<vector<char> > board;
    string str1="ABCE";
    string str2="SFES";
    string str3="ADEE";

    vector<char> tmp1(str1.begin(),str1.end());
    vector<char> tmp2(str2.begin(),str2.end());
    vector<char> tmp3(str3.begin(),str3.end());
    
    board.push_back(tmp1);
    board.push_back(tmp2);
    board.push_back(tmp3);
    cout<<exist(board,"ABCESEEEFS")<<endl;
    
    return 0;
}




