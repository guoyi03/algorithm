/*
 * Author     : Guo Yi
 * Create Time: 2013-10-18
 * Problem    : Pascal's Triangle II
 *              Given an index k, return the kth row of the Pascal's triangle.
 *              http://oj.leetcode.com/problems/pascals-triangle-ii/
 * Solution   : cycle
 *
 */


#include<iostream>
#include<vector>
using namespace std;

vector<int> getRow(int numIndex) {
    // Note: The Solution object is instantiated only once and is reused by each test case.
    
    vector<int>  now;
    vector<int>  past;
    int i =0;
    int j =0;
    now.push_back(1);

    if(numIndex == 0)return  now;

    for(i =1 ;i<=numIndex; i++)
    {
        past = now;
        now.clear();
        now.push_back(1);
        for(j = 1 ;j< i; j++)
        {
            now.push_back(past[j-1]+ past[j]);
        }
        now.push_back(1);
    }
    return now;
}
int main()
{
    getRow(5);
    return 0;
}




