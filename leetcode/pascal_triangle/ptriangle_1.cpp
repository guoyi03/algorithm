/*
 * Author     : Guo Yi
 * Create Time: 2013-10-18
 * Problem    : Pascal's Triangle
 *              Given numRows, generate the first numRows of Pascal's triangle.
 *              http://oj.leetcode.com/problems/pascals-triangle/
 * Solution   : cycle
 *
 */


#include<iostream>
#include<vector>
using namespace std;

vector<vector<int> > generate(int numRows) {
    // Note: The Solution object is instantiated only once and is reused by each test case.
    
    vector<vector<int> > result;
    int i =0;
    int j =0;
    if(numRows == 0)return  result;

    vector<int> tmp1;
    tmp1.push_back(1);
    result.push_back(tmp1);

    for(i =1 ;i<numRows; i++)
    {
        vector<int>temp;
        temp.push_back(1);
        for(j = 1 ;j< i; j++)
        {
            temp.push_back(result[i-1][j-1]+ result[i-1][j]);
        }
        temp.push_back(1);
        result.push_back(temp);
    }
    return result;
}
int main()
{
    generate(5);
    return 0;
}




