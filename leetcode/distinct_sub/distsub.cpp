/*
 * Author     : Guo Yi
 * Create Time: 2013.10.20
 * Problem    : Given a string S and a string T, count the number of distinct subsequences of T in S.
 *              http://oj.leetcode.com/problems/distinct-subsequences/
 * Solution   : dynamic programming
 *
 */


#include<iostream>
#include<string>
#include<algorithm>
using namespace std;



int **allocmem(int sizex, int sizey)
{
    int ** result=new int*[sizex];
    for(int i = 0; i < sizex; ++i)
            result[i] = new int[sizey];
    return result;
}

void freemem(int ** result, int sizex)
{
    for (int i = 0; i < sizex; ++i)
    {
        delete result[i];
        result[i] = NULL;
    }
}


int numDistinct(string S, string T) {

    int ret;
    int i=0;
    int j=0;
    if(S.empty()||T.empty())return 0;
    
    if(S.size()< T.size())return 0;

    if(S==T) return 1;

    
    //results;
    int** result = allocmem(S.size(),T.size()); //i means S[0,i]; j means T[0,j];

    for(i=0; i< T.size(); i++)
    {
//        cout<<i<<" "<<S.substr(0,i+1) <<" " <<T.substr(0,i+1);
        if(S.substr(0,i+1)==T.substr(0,i+1))
            result[i][i]=1;
        else
            result[i][i]=0;
//        cout<<" "<<result[i][i] <<endl;
    }
    

    for(i = 1; i< S.size(); i++)
    {
        int limit = min(i,(int)T.size());
        for(j=0; j< limit ; j++)
        {
            if(S[i]==T[j])
            {
                if(j==0)
                    result[i][j] = result[i-1][j] +1;
                else
                    result[i][j] = result[i-1][j] + result[i-1][j-1];
            }
            else
                result[i][j] = result[i-1][j] ;
//            cout<< result[i][j] << " ";
        }
#if 0
        if(limit <T.size())
            cout<< result[i][i] << endl;
        else
            cout<< endl;
#endif
    }
    
    ret = result[S.size()-1][T.size()-1];
    freemem(result,S.size());
    return ret;
}

int main()
{
    string str ("ccc");
    string str2 ("c");
    cout<< numDistinct(str, str2) <<endl;
    return 0;
}




