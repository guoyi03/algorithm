/*
 * Author     : Guo Yi
 * Create Time: 2013.10.27
 * Problem    : Restore IP addresses
 *              Given a string containing only digits, restore it by returning all possible valid IP address combinations.
 *              http://oj.leetcode.com/problems/restore-ip-addresses/
 *
 * Solution   : just search, find three positions that the three dots in.
 *
 */


#include<iostream>
#include<string>
#include<vector>
#include <stdlib.h>
using namespace std;


bool isIP(string s)
{
    if(s.empty()||s.size()>3)return false;
    if(s.size()==3)
    {
        if(s[0]>'2')return false;
        if(s[0]=='2'&&(atoi(s.substr(1,2).c_str())>55))return false;
        if(s[0]=='0')return false;
        if(s.substr(0,2)=="00")return false;
    }
    if(s.size()==2&&s[0]=='0')return false;
    return true;
}

vector<string> restoreIpAddresses(string s) {
    
    int cur1,cur2,cur3;
    vector<string> result;

    result.clear();
    if(s.empty())return result;
    if(s.size()<4) return result;
    
    for(int i= 0; i<3;i++) //first
    {
        if(isIP(s.substr(0,i+1))) 
        {
            for(int j=i+1; j< i+4 && j<s.size()-2;j++) //second
            {
                if(isIP(s.substr(i+1,j-i)))
                {
                    for(int k=j+1; k<j+4 && k<s.size()-1;k++) //third
                    {
                        if(s.size()-k-1>3)continue;
                        if(isIP(s.substr(j+1,k-j))&&isIP(s.substr(k+1,s.size()-k-1)))
                        {
                            result.push_back(s.substr(0,i+1)+"."+s.substr(i+1,j-i)+"."+s.substr(j+1,k-j)+"."+s.substr(k+1,s.size()-k-1));
                        }
                    }
                }
            }
        }
    }
    return result;
}
int main()
{
    string s1 = "19216811";
    restoreIpAddresses(s1);
    return 0;
}




