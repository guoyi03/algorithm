/*
 * Author     : Guo Yi
 * Create Time: 2013.11.29
 * Problem    : Unique Paths II
 *              A robot is located at the top-left corner of a m x n grid
 *              How many possible unique paths to move to the bottom-right conner?
 *              (some obstacles are added to the grids)
 *              http://oj.leetcode.com/problems/unique-paths/
 * Solution   : DP
 *
 */


#include<iostream>
#include<vector>
using namespace std;

int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {

    if(obstacleGrid.empty()) return 0;
    int m = obstacleGrid.size();
    int n = obstacleGrid[0].size();
    if(m==0||n==0) return 0;
    int i,j;
    int res;
    if(obstacleGrid[0][0]==1)return 0;
    int **result=new int*[m];
    for(i = 0; i < m; ++i)
        result[i] = new int[n];
    
    if(obstacleGrid[0][0]==0)
        result[0][0] = 1;
    else
        result[0][0] = 0;
    //result[i][j]=result[i-1][j]+result[i][j-1];
    
    for(i = 1; i<m ;i++)
    {
        if(obstacleGrid[i][0]==0)
            result[i][0] = 1;
        else
        {
            while(i<m)
            {
                result[i][0] = 0;
                i++;
            }
            break;
        }
    }
    for(i =1; i<n ;i++)
    {
        if(obstacleGrid[0][i]==0)
            result[0][i] = 1;
        else
        {
            while(i<n)
            {
                result[0][i] =0;
                i++;
            }
            break;
        }
    }

    for(i=1; i<m ;i++)
        for(j=1; j<n ;j++)
        {
            if(obstacleGrid[i][j]==1)
                result[i][j] =0;
            else
                result[i][j] = result[i-1][j] + result[i][j-1];
        }
    res = result[m-1][n-1];

    for (i = 0; i < m; ++i)
    {
        delete result[i];
        result[i] = NULL;
    }

    return res;
}
int main()
{
    vector<int> tmp1;
    vector<int> tmp2;
    vector<int> tmp3;

    tmp1.push_back(0);
    tmp1.push_back(0);
    tmp1.push_back(0);
    tmp2.push_back(0);
    tmp2.push_back(1);
    tmp2.push_back(0);
    tmp3.push_back(0);
    tmp3.push_back(0);
    tmp3.push_back(0);

    vector<vector<int> > obstacleGrid;
    obstacleGrid.push_back(tmp1);
    obstacleGrid.push_back(tmp2);
    obstacleGrid.push_back(tmp3);

    cout<< uniquePathsWithObstacles(obstacleGrid)<<endl;
    return 0;
}




