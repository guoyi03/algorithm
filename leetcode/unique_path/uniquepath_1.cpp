/*
 * Author     : Guo Yi
 * Create Time: 2013.11.29
 * Problem    : Unique Paths
 *              A robot is located at the top-left corner of a m x n grid
 *              How many possible unique paths to move to the bottom-right conner?
 *              http://oj.leetcode.com/problems/unique-paths/
 * Solution   : DP
 *
 */


#include<iostream>
using namespace std;

int uniquePaths(int m, int n) {

    if(m==0||n==0) return 0;
    int i,j;
    int res;
    int **result=new int*[m];
    for(i = 0; i < m; ++i)
        result[i] = new int[n];
    
    result[0][0] = 1;
    //result[i][j]=result[i-1][j]+result[i][j-1];
    
    for(i = 1; i<m ;i++)
        result[i][0] = 1;
    for(i =1; i<n ;i++)
        result[0][i] = 1;

    for(i=1; i<m ;i++)
        for(j=1; j<n ;j++)
            result[i][j] = result[i-1][j] + result[i][j-1];
    res = result[m-1][n-1];

    for (i = 0; i < m; ++i)
    {
        delete result[i];
        result[i] = NULL;
    }

    return res;
}
int main()
{
    cout<<uniquePaths(3,7)<<endl;
    return 0;
}




