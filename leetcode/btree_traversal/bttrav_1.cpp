/*
 * Author     : Guo Yi
 * Create Time: 2013.10.23 
 * Problem    : Binary Tree Level Order Traversal
 *              Given a binary tree, return the level order traversal of its nodes' values
 *
 * Solution   : BFS (queue)
 *              when use map to store the treenode and level relevance, the result shows:Memory Limit Exceeded
 *              so use queue
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


queue <TreeNode*> treeQueue;
//map<TreeNode*,int> treeMap;
queue <int> treeMap;

vector<vector<int> > levelOrder(TreeNode *root) {
    vector<vector<int> >result;
    int level = 0;
    int lastlevel = 0;
    if(root==NULL) return result;
    
    TreeNode * tnode;
    
    treeQueue.push(root);
    treeMap.push(level);
//    treeMap.insert(pair<TreeNode*,int>(root, 0));
    tnode = root;
    treeQueue.pop();
    treeMap.pop();

    while(tnode)
    {
        vector<int> temp;
//        while(tnode && treeMap[tnode]==level)
        while(tnode && lastlevel==level)
        {
            temp.push_back(tnode->val);
            if(tnode->left)
            {
                treeQueue.push(tnode->left);
                treeMap.push(lastlevel+1);
    //            treeMap.insert(pair<TreeNode*,int>(tnode->left,treeMap[tnode]+1));
            }
            if(tnode->right)
            {
                treeQueue.push(tnode->right);
                treeMap.push(lastlevel+1);
                //treeMap.insert(pair<TreeNode*,int>(tnode->right,treeMap[tnode]+1));
            }
            if(!treeQueue.empty())
            {
                tnode = treeQueue.front();
                treeQueue.pop();
                lastlevel = treeMap.front();
                treeMap.pop();
            }
            else
                tnode =  NULL;
        }
        result.push_back(temp);
#ifdef DEBUG        
        cout<<"in level "<< level<< ":";
        for(int i =0 ; i< temp.size();i++)
            cout<<temp[i]<<" ";
        cout<< endl;
#endif
        level++;
    }
    return result;
}

int main()
{
    TreeNode* t1 = new TreeNode(3);
    TreeNode* t2 = new TreeNode(9);

    t1->right = t2;

    levelOrder(t1);
    
    return 0;
}




