/*
 * Author     : Guo Yi
 * Create Time: 2013.10.23 
 * Problem    : Binary Tree Zigzag Level Order Traversal
 *              Given a binary tree, return the zigzag level order traversal of its nodes' values
 *
 * Solution   : BFS (queue)
 *              when use map to store the treenode and level relevance, the result shows:Memory Limit Exceeded
 *              so use queue
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


queue <TreeNode*> treeQueue;
queue <int> treeMap;

vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
    vector<vector<int> >result;
    int level = 0;
    int lastlevel = 0;

    int currorder =0; //0 : right to left; 1: left to right

    if(root==NULL) return result;
    
    TreeNode * tnode;
    
    treeQueue.push(root);
    treeMap.push(level);
    tnode = root;
    treeQueue.pop();
    treeMap.pop();

    while(tnode)
    {
        vector<int> temp;
        while(tnode && lastlevel==level)
        {
            if(currorder==0)
                temp.push_back(tnode->val);
            else
                temp.insert(temp.begin(),tnode->val);
            if(tnode->left)
            {
                treeQueue.push(tnode->left);
                treeMap.push(lastlevel+1);
            }
            if(tnode->right)
            {
                treeQueue.push(tnode->right);
                treeMap.push(lastlevel+1);
            }
            if(!treeQueue.empty())
            {
                tnode = treeQueue.front();
                treeQueue.pop();
                lastlevel = treeMap.front();
                treeMap.pop();
            }
            else
                tnode =  NULL;
        }
        if(currorder ==0 )currorder =1;
        else currorder =0;
        result.push_back(temp);
#ifndef DEBUG        
        cout<<"in level "<< level<< ":";
        for(int i =0 ; i< temp.size();i++)
            cout<<temp[i]<<" ";
        cout<< endl;
#endif
        level++;
    }
    return result;
}

int main()
{
    TreeNode* t1 = new TreeNode(3);
    TreeNode* t2 = new TreeNode(9);

    t1->right = t2;

    zigzagLevelOrder(t1);
    
    return 0;
}




