/*
 * Author     : Guo Yi
 * Create Time: 2013.10.09  
 * Problem    : Longest Consecutive Sequence
 *              Given an unsorted array of integers, 
 *              find the length of the longest consecutive elements sequence.
 *              http://oj.leetcode.com/problems/longest-consecutive-sequence/
 * Solution   : O(n) (find-->erase)
 *              g++ lcs.cpp  -std=c++0x
 */


#include<iostream>
#include<vector>
#include<unordered_set>

using namespace std;

unordered_set<int> tcopy;

int findnext(int value, int direction)
{
    int maxlen = 0;
    while(tcopy.find(value)!=tcopy.end())
    {
        tcopy.erase(value);
        maxlen++;
        if(direction==0)//desc
            value --;    
        else //asec
            value ++;
    }
    return maxlen;
}

int longestConsecutive(vector<int> &num) {
    int maxvalue = 0;
    int desc=0;
    int aesc=0;

    if(num.empty())return 0;
    for(int i  =0 ;i<num.size(); i++)
        tcopy.insert(num[i]);
    for(int i = 0 ; i<num.size(); i ++)
    {
        desc = findnext(num[i]-1,0);
        aesc = findnext(num[i],1);
        if(maxvalue<desc+aesc)
            maxvalue = desc+aesc;
    }
    return maxvalue;

}
int main()
{

    vector<int> temp;
    temp.push_back(100);
    temp.push_back(2);
    temp.push_back(200);
    temp.push_back(3);
    cout << longestConsecutive(temp) <<endl;
    return 0;
}




