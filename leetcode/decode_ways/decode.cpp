/*
 * Author     : Guo Yi
 * Create Time: 2013.10.31
 * Problem    : Decode Ways
 *              http://oj.leetcode.com/problems/decode-ways/
 *
 * Solution   : divide and conquer
 *
 */


#include<iostream>
#include<string>
#include<map>
using namespace std;

map<string,string> hashmap;
void createMap(){
    hashmap.clear();
    hashmap["1"] ="A";
    hashmap["2"] ="B";
    hashmap["3"] ="C";
    hashmap["4"] ="D";
    hashmap["6"] ="F";
    hashmap["7"] ="G";
    hashmap["8"] ="H";
    hashmap["9"] ="I";
    hashmap["10"] ="J";
    hashmap["11"] ="K";
    hashmap["12"] ="L";
    hashmap["13"] ="M";
    hashmap["14"] ="N";
    hashmap["15"] ="O";
    hashmap["16"] ="P";
    hashmap["17"] ="Q";
    hashmap["18"] ="R";
    hashmap["19"] ="S";
    hashmap["20"] ="T";
    hashmap["21"] ="U";
    hashmap["22"] ="V";
    hashmap["23"] ="W";
    hashmap["24"] ="X";
    hashmap["25"] ="Y";
    hashmap["26"] ="Z";
}

int numDecodings(string s) {
    createMap();
    if(s.empty()) return 0;
    int *result = new int[s.size()];
    int res;
    if(s[0]=='0')return 0;

    result[0] = 1;
    for(int i = 1 ;i<s.size(); i++)
    {
        result[i] = 0;
        if(!hashmap[s.substr(i-1,2)].empty())
        {
            if(i==1)
                result[i] = 1;
            else
                result[i] = result[i-2];
        }
        if(s[i]!='0')
            result[i] =  result[i] + result[i-1];
    }
    res = result[s.size()-1];
    delete result;
    return res;
}

int main()
{
    string s ="12";
    cout<< numDecodings(s)<<endl;
    return 0;
}





