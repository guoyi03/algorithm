/*
 * Author     : Guo Yi
 * Create Time: 2013.10.05
 * Problem    : Surrounded Regions 
 *              http://oj.leetcode.com/problems/surrounded-regions/
 * Solution   : DFS, find the position that can't surrounded
 *                   reverse thinking
 */
#include <iostream>
#include <cassert>
#include <vector>
using namespace std;

vector <vector<char> > tcopy ;
vector <vector<char> > flag;
int xsize;
int ysize;
//DFS
void mark(int i, int j)
{
    if(i-1>=0) //(i-1,j)
    {
        if(tcopy[i-1][j]=='O' && flag[i-1][j]!='Y') //
        {
            flag[i-1][j]='Y';
            mark(i-1,j);
        }
    }
    if(j-1>=0) //(i,j-1)
    {
        if(tcopy[i][j-1]=='O' && flag[i][j-1]!='Y') //
        {
            flag[i][j-1]='Y';
            mark(i,j-1);
        }
    }
    if(j+1<xsize) //(i,j+1)
    {
        if(tcopy[i][j+1]=='O' && flag[i][j+1]!='Y') //
        {
            flag[i][j+1]='Y';
            mark(i,j+1);
        }
    }
    if(i+1<ysize) //(i+1,j)
    {
        if(tcopy[i+1][j]=='O' && flag[i+1][j]!='Y') //
        {
            flag[i+1][j]='Y';
            mark(i+1,j);
        }
    }
}
void solve(vector<vector<char> > &board) {
    tcopy =  board;
    flag = board;
    vector<char> ctemp1;
    vector<int> itemp;
    int i,j;

    if(board.empty()) return;
    ysize =  board.size();

    if(ysize<=2) return;
    
    ctemp1 = board[0];

    xsize = ctemp1.size();

    if(xsize<=2) return ;
    
    for(i = 0; i<xsize; i++)
    {
        if(board[0][i]=='O' && flag[0][i]!='Y')
        { 
            flag[0][i]='Y';
            mark(0,i);
        }
        if(board[ysize-1][i]=='O' && flag[ysize-1][i]!='Y')
        {
            flag[ysize-1][i]='Y';
            mark(ysize-1,i);
        }
    }
    for(i=1 ; i <ysize-1;i++)
    {
        if(board[i][0]=='O' && flag[i][0]!='Y')
        {
            flag[i][0]='Y';
            mark(i,0);     
        }
        if(board[i][xsize-1]=='O' && flag[i][xsize-1]!='Y')
        {
            flag[i][xsize-1]='Y';
            mark(i,xsize-1);     
        }
    }
    for(i=0;i<ysize;i++)
    {
        for(j=0;j<xsize;j++)
        {
            cout << board[i][j] << " ";
            if(flag[i][j]!='Y')
                board[i][j]='X';
            //cout << board[i][j] << " ";
        }
        cout << endl;
    }
}
int main()
{
    vector<char> v1;
    v1.push_back('O');
    v1.push_back('X');
    v1.push_back('X');
    v1.push_back('X');
    vector<char> v2;
    v2.push_back('O');
    v2.push_back('X');
    v2.push_back('O');
    v2.push_back('X');
    vector<char> v3;
    v3.push_back('O');
    v3.push_back('X');
    v3.push_back('O');
    v3.push_back('X');
    vector<char> v4;
    v4.push_back('O');
    v4.push_back('X');
    v4.push_back('X');
    v4.push_back('X');

    vector< vector<char> > board;
    board.push_back(v1);
    board.push_back(v2);
    board.push_back(v3);
    board.push_back(v4);
    cout<< board[0][0] <<endl; 
    solve(board);

    return 0;
}
