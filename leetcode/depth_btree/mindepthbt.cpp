/*
 * Author     : Guo Yi
 * Create Time: 2013.10.22
 * Problem    : Minimum Depth of Binary Tree 
 *              Given a binary tree, find its minimum depth.
 *              http://oj.leetcode.com/problems/minimum-depth-of-binary-tree/
 *
 * Solution   : DFS, just the same as path sum
 *
 */


#include<iostream>
#include<stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
stack<int> st; //store flag,  1 means we have scan node's right child
                            //0 means we have scan node's left child
                            //if 1, we should not scan this node ever
stack<TreeNode*> q;

//DFS
int minDepth(TreeNode *root) {
    int mindep = 1000000;
    int tmpsum = 0;
    TreeNode * cur ;
    TreeNode * tmp;

    if(root==NULL)return 0;

    cur = root;

    while(cur || !q.empty())
    {
        while(cur!=NULL)
        {
            tmpsum = tmpsum + 1;

            q.push(cur);
            st.push(0);
            cur =  cur->left;
        }
        if(!q.empty())
        {
            cur = q.top();
            st.pop();
            st.push(1); //find right;

            tmp = cur;
            cur = cur->right;
            if(cur == NULL) 
            {
                if(tmp->left ==NULL) //leaf node
                {
                    mindep = min(mindep,tmpsum);
                }
                while(!st.empty()&&st.top()==1)
                {
                    tmpsum = tmpsum-1;
                    st.pop();
                    q.pop();
                    if(!q.empty())
                        tmp = q.top();
                }
            }
        }
    }
    return mindep;
    
}
int main()
{
    TreeNode *n1 = new TreeNode(7);
    TreeNode *n2 = new TreeNode(7);
    TreeNode *n3 = new TreeNode(4);
    TreeNode *n4 = new TreeNode(4);
    n1->right = n2;
    n2->right = n3;
//    n2->right = n4;
    cout<<minDepth(n1)<<endl;

    delete n1,n2,n3,n4;
    return 0;
}


