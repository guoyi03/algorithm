/*
 * Author     : Guo Yi
 * Create Time: 2013.11.27
 * Problem    : Minimum Path Sum
 *              Given a m x n grid filled with non-negative numbers,
 *              find a path from top left to bottom right which minimizes the sum of all numbers along its path.
 *              You can only move either down or right at any point in time.
 *              http://oj.leetcode.com/problems/minimum-path-sum/
 *
 * Solution   : DP
 *              (if you can move left or up, this problem becomes more difficult, 
 *              we should use Dijkstra algorithm to solve this one(m*n nodes) )
 *
 */


#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;

int minPathSum(vector<vector<int> > &grid) {
   if(grid.empty())return 0;

   int xsize = grid.size();
   int ysize = grid[0].size();
   int i=0;
   int j=0;
   vector<vector<int> > minsum = grid;
   for(i=0; i<xsize; i++)
       for(j=0;j<ysize; j++)
           minsum[i][j] = INT_MAX;

   minsum[0][0] = grid[0][0];

   for(i = 1; i < xsize; i++)
       minsum[i][0] = minsum[i-1][0]+grid[i][0];

   for(i = 1; i < ysize; i++)
       minsum[0][i] = minsum[0][i-1] + grid[0][i];

   
    for(i=1; i<xsize; i++)
       for(j=1;j<ysize; j++)
           minsum[i][j] = min(minsum[i-1][j],minsum[i][j-1]) + grid[i][j];

    return minsum[xsize-1][ysize-1];
}

int main()
{
    vector<int> tmp;
    tmp.push_back(1);
    tmp.push_back(1);
    tmp.push_back(1);
    
    vector<int> tmp1;
    tmp1.push_back(2);
    tmp1.push_back(3);
    tmp1.push_back(4);

    vector<vector<int> > grid;
    grid.push_back(tmp);
    grid.push_back(tmp1);

    cout<<minPathSum(grid) <<endl;

    return 0;
}




