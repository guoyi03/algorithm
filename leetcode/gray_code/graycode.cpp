/*
 * Author     : Guo Yi
 * Create Time: 2013.10.31
 * Problem    : Gray Code
 *              http://oj.leetcode.com/problems/gray-code/
 *
 * Solution   : divide and conquer
 *
 */


#include<iostream>
#include<vector>
using namespace std;


vector<int> grayCode(int n) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    vector<int> result;
    if(n==0) {
        result.push_back(0);
        return result;
    }
    if(n==1) 
    {
        result.push_back(0);
        result.push_back(1);
        return result;
    }
    result = grayCode(n-1);
    int size = result.size();
    int high = 1<<(n-1);
    for(int i= size-1; i>=0 ;i--)
    {
        result.push_back(result[i]+high);
    }
    return result;
}
int main()
{
    vector<int> res;
    res = grayCode(2);
    for(int i =0 ;i<res.size() ;i++)
        cout<< res[i] <<endl;
    return 0;
}




