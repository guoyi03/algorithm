/*
 * Author     : Guo Yi
 * Create Time: 2013.11.14
 * Problem    : Search in Rotated Sorted Array II
 *              Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 *              You are given a target value to search. If found in the array return its index, otherwise return -1.
 *              http://oj.leetcode.com/problems/search-in-rotated-sorted-array-ii/
 *              duplicates are allowed
 *
 * Solution   : O(n)
 *              if we use binary search ,
 *              we have the computational complexity of O(logn)
 *
 */


#include<iostream>
using namespace std;

bool search(int A[], int n, int target) {    
    int i =0;

    for(i =0 ;i<n; i++)
    {
        if(target == A[i]) return i;
    }
    return -1;
}
int main()
{
    int A[4]={3,4,1,2};
    cout<< search(A,4,1) <<endl;
    return 0;
}




