/*
 * Author     : Guo Yi
 * Create Time: 2013.12.2
 * Problem    : Insert Interval
 *              Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).
 *
 *
 * Solution   : search
 *
 */


#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
    vector<Interval> result;

    if(intervals.empty())
    {
        result.push_back(newInterval);
        return result;
    }
    int size = intervals.size();
    int i =0;
    int tstart = 0; 
    int tend = 0;
    int flag = 0; //not insert

    if(newInterval.start <  intervals[0].start ) //merge first
    {
        tstart = newInterval.start;
        flag = 1; //inserting
    }
    if(newInterval.end < intervals[0].start)
    {
        tend = newInterval.end;
        Interval tmp(tstart,tend);
        result.push_back(tmp);
        flag = 2; // inserted
    }

    for(i=0; i< size-1 ;i++)
    {
        if(flag ==2 )
        {
            result.push_back(intervals[i]);
            continue;
        }
        if(newInterval.start >= intervals[i].start && newInterval.start <= intervals[i].end)
        {
            tstart = intervals[i].start;
            flag = 1;
        }
        if(newInterval.end >= intervals[i].start && newInterval.end <= intervals[i].end)
        {
            tend = intervals[i].end;
            Interval tmp(tstart,tend); 
            result.push_back(tmp);
            flag = 2;
        }
        if(flag == 0)
            result.push_back(intervals[i]); 

         if(newInterval.start > intervals[i].end && newInterval.start < intervals[i+1].start)
         {
             tstart = newInterval.start;
             flag = 1;
         }
         if(newInterval.end > intervals[i].end && newInterval.end < intervals[i+1].start)
         {
             tend = newInterval.end;
             Interval tmp(tstart,tend);
             result.push_back(tmp);
             flag = 2;
         }
    }
    i = size -1;
    if(newInterval.start >= intervals[i].start && newInterval.start <= intervals[i].end)
    {
        tstart = intervals[i].start;
        flag = 1;
    }
    if(newInterval.end >= intervals[i].start && newInterval.end <= intervals[i].end)
    {
        tend = intervals[i].end;
        Interval tmp(tstart,tend); 
        result.push_back(tmp);
        flag = 2;
        return result;
    }
    
    if(flag==0)
    {
        result.push_back(intervals[i]);
        result.push_back(newInterval);
    }
    if(flag==1)
    {
        Interval tmp(tstart,newInterval.end);
        result.push_back(tmp);
    }
    if(flag ==2)
        result.push_back(intervals[i]);
    return result;
}
int main()
{
    Interval tmp0(1,2);
    Interval tmp1(3,5);
    Interval tmp2(6,7);
    Interval tmp3(8,10);
    Interval tmp4(13,16);
    vector<Interval> interval;
    interval.push_back(tmp0);
    interval.push_back(tmp1);
    interval.push_back(tmp2);
    interval.push_back(tmp3);
    interval.push_back(tmp4);
    vector<Interval> result;
    int i =0;
    
    Interval ins(4,9);
    result = insert(interval,ins);
    for(i=0;i<result.size() ;i++)
        cout<<result[i].start<<" "<<result[i].end<<endl;
    cout<<"----------------" <<endl;

    Interval ins1(-1,0);
    result = insert(interval,ins1);
    for(i=0;i<result.size() ;i++)
        cout<<result[i].start<<" "<<result[i].end<<endl;


    return 0;
}




