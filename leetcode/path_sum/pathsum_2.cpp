/*
 * Author     : Guo Yi
 * Create Time: 2013.10.21
 * Problem    : Given a binary tree and a sum
 *              find all root-to-leaf paths where each path's sum equals the given sum.
 *              http://oj.leetcode.com/problems/path-sum-ii/
 * Solution   : Traverse Binary Tree (DFS)
 *
 */


#include<iostream>
#include<stack>
#include<vector>
#include<algorithm>
using namespace std;

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

stack<int> st; //store flag,  1 means we have scan node's right child
                            //0 means we have scan node's left child
                            //if 1, we should not scan this node ever
stack<TreeNode*> q;

vector<int> copystack()
{
    vector<int>tmp;
    vector<TreeNode*>ttmp;
    while(!q.empty())
    {
        tmp.push_back(q.top()->val);
        ttmp.push_back(q.top());
        q.pop();
    }
    reverse(tmp.begin(),tmp.end());
    reverse(ttmp.begin(),ttmp.end());

    for(int i =0; i<ttmp.size();i++)
    {
        q.push(ttmp[i]);
    }
    return tmp;
}

//DFS
vector<vector<int> > pathSum(TreeNode *root, int sum) {
    int sumnum =0;
    int tmpsum = 0;
    TreeNode * cur ;
    TreeNode * tmp;
    
    vector< vector<int> > result;


    if(root==NULL)return result;

    cur = root;

    while(cur || !q.empty())
    {
        while(cur!=NULL)
        {
            tmpsum = tmpsum + cur->val;

            q.push(cur);
            st.push(0);
            cur =  cur->left;
        }
        if(!q.empty())
        {
            cur = q.top();
            st.pop();
            st.push(1); //find right;

            tmp = cur;
            cur = cur->right;
            if(cur == NULL) 
            {
                if(tmp->left ==NULL && tmpsum == sum) //leaf node
                {
                    result.push_back( copystack() );
                }
                while(!st.empty()&&st.top()==1)
                {
                    tmpsum = tmpsum-tmp->val;
                    st.pop();
                    q.pop();
                    if(!q.empty())
                        tmp = q.top();
                }
            }
        }
    }
    return result;
    
}
int main()
{
    TreeNode *n1 = new TreeNode(7);
    TreeNode *n2 = new TreeNode(7);
    TreeNode *n3 = new TreeNode(4);
    TreeNode *n4 = new TreeNode(4);
    n1->right = n2;
    n2->right = n3;
    vector< vector<int> > res = pathSum(n1,18);
    delete n1,n2,n3,n4;
    return 0;
}




