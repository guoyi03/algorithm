/*
 * Author     : Guo Yi
 * Create Time: 2013.10.21
 * Problem    : Given a binary tree and a sum
 *              determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum
 *              http://oj.leetcode.com/problems/path-sum/
 * Solution   : Traverse Binary Tree (DFS)
 *
 */


#include<iostream>
#include<stack>
using namespace std;

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

stack<int> st; //store flag,  1 means we have scan node's right child
                            //0 means we have scan node's left child
                            //if 1, we should not scan this node ever
stack<TreeNode*> q;

//DFS
bool hasPathSum(TreeNode *root, int sum) {
    int sumnum =0;
    int tmpsum = 0;
    TreeNode * cur ;
    TreeNode * tmp;

    if(root==NULL)return 0;

    cur = root;

    while(cur || !q.empty())
    {
        while(cur!=NULL)
        {
            tmpsum = tmpsum + cur->val;

            q.push(cur);
            st.push(0);
            cur =  cur->left;
        }
        if(!q.empty())
        {
            cur = q.top();
            st.pop();
            st.push(1); //find right;

            tmp = cur;
            cur = cur->right;
            if(cur == NULL) 
            {
                if(tmp->left ==NULL && tmpsum == sum) //leaf node
                    return true;
                while(!st.empty()&&st.top()==1)
                {
                    tmpsum = tmpsum-tmp->val;
                    st.pop();
                    q.pop();
                    if(!q.empty())
                        tmp = q.top();
                }
            }
        }
    }
    return false;
    
}
int main()
{
    TreeNode *n1 = new TreeNode(7);
    TreeNode *n2 = new TreeNode(7);
    TreeNode *n3 = new TreeNode(4);
    TreeNode *n4 = new TreeNode(4);
    n1->right = n2;
    n2->right = n3;
//    n2->right = n4;
    cout<<hasPathSum(n1,18)<<endl;

    delete n1,n2,n3,n4;
    return 0;
}




