/*
 * Author     : Guo Yi
 * Create Time: 2013.11.19
 * Problem    : Edit distance
 *              Given two word word1 and word2,
 *              find the minimum number of steps required to convert word1 to word2
 *              a) Insert a character
 *              b) Delete a character
 *              c) Replace a character
 *
 * Solution   : Dynamic programing
 *
 */


#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;

int minDistance(string word1, string word2) {
    
    if(word1.empty())return word2.size();
    if(word2.empty())return word1.size();
    int fromsize = word1.size();
    int tosize = word2.size();

    int i=0;
    int j=0;
    vector< vector<int> > dist;

    for(i =0 ;i<=fromsize; i++)
    {
        vector<int> tmp;
        for(j=0; j<=tosize; j++)
        {
            if(j==0)
                tmp.push_back(i);
            else
            {
                if(i==0)
                    tmp.push_back(j);
                else
                    tmp.push_back(UINT_MAX-1);
            }
        }
        dist.push_back(tmp);
    }

    int step0,step1,step2;
    for(i =1;i<=fromsize ;i++)
    {
        for(j=1; j<=tosize; j++)
        {
            step0 = dist[i-1][j]+1;
            step1 = dist[i][j-1]+1;
            if(word1[i-1]==word2[j-1])
                step2 = dist[i-1][j-1];
            else
                step2 = dist[i-1][j-1]+1;
            dist[i][j] = min(min(step0,step1),step2);
        }
    }
    return dist[fromsize][tosize];

}
int main()
{
    string word1="sea";
    string word2="eat";
    cout<<minDistance(word1, word2)<<endl;
    return 0;
}




