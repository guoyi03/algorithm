/*
 * Author     : Guo Yi
 * Create Time: Valid Palindrome
 * Problem    : Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
 *              http://oj.leetcode.com/problems/valid-palindrome/
 * Solution   : search
 *
 */


#include<iostream>
#include<string>
using namespace std;

 bool isPalindrome(string s) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        int i =0;
        int j =0;
        string word;
        if(s.size()==0) return true;
        for(i = 0;i < s.size();i++)
        {

            if(!isalpha(s[i]) && !isdigit(s[i]))continue;
            
            if(isupper(s[i]))
                word += tolower(s[i]);
            else
                word += s[i];
        }
        //cout << word.size() <<" dddd "<<word <<endl; 
        for(i = 0 ; i<word.size()/2; i++)
        {
            if(word[i]!=word[word.size()-i-1])
                return false;
        }
        return true;
    }

int main()
{

    string s = "A man, a plan, a canal: Panama";
    string s1 = "1a2";
    cout << isPalindrome(s) <<endl;
    cout << isPalindrome(s1) <<endl;
    return 0;
}




