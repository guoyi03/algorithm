/*
 * Author     : Guo Yi
 * Create Time: 2013.11.18
 * Problem    : Search Matrix Zeroes
 *              Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.
 *              http://oj.leetcode.com/problems/set-matrix-zeroes/
 * Solution   : Use UINT_MAX to sign zeroes that were exist in origin matrix
 *
 */


#include<iostream>
#include<vector>
#include <limits.h>
using namespace std;
void setZeroes(vector<vector<int> > &matrix) {

    if(matrix.empty()) return;
    int xsize = matrix.size();
    int ysize = matrix[0].size();

    int i,j;

    int k,l;

    for(i=0; i<xsize; i++)
    {
        for(j=0; j<ysize; j++)
        {
            if(matrix[i][j]==0)
            {
                k = j;
                while(k>=0)
                {
                    if(matrix[i][k]==0)
                        matrix[i][k] =UINT_MAX;
                    else
                        matrix[i][k] =0;
                    k--;
                }
                k= j+1;
                while(k<ysize)
                {
                    if(matrix[i][k]==0)
                        matrix[i][k]= UINT_MAX;
                    else
                        matrix[i][k]=0;
                    k++;
                }
                break;
            }
        }
    }
    for(i=0 ;i < ysize; i++)
        for(j=0; j<xsize; j++)
        {
            if(matrix[j][i]==UINT_MAX)
            {
                k = j;
                while(k>=0)
                {
                    matrix[k][i] =0;
                    k--;
                }
                k= j+1;
                while(k<xsize)
                {
                    matrix[k][i]=0;
                    k++;
                }
                break;
            }
        }
    return;
}
int main()
{

    vector<vector<int> > matrix;
    vector<int> tmp;
    tmp.push_back(0);
    tmp.push_back(1);
    matrix.push_back(tmp);

    setZeroes(matrix);
    return 0;
}




