/*
 * Author     : Guo Yi
 * Create Time: 2013.11.18
 * Problem    : Search a 2D Matrix 
 *              Write an efficient algorithm that searches for a value in an m x n matrix. 
 *              This matrix has the following properties :
 *                  Integers in each row are sorted from left to right.
 *                  The first integer of each row is greater than the last integer of the previous row.
 *              http://oj.leetcode.com/problems/search-a-2d-matrix/
 * Solution   :
 *
 */


#include<iostream>
#include<vector>
using namespace std;

bool searchMatrix(vector<vector<int> > &matrix, int target) {
    
    if(matrix.empty()) return false;
    int xsize = matrix.size();
    int ysize = matrix[0].size();

    int i,j;

    for(i=0; i<xsize; i++)
    {
        if(matrix[i][0]>target) break;
    }
    if(i==0)return false;
    
    for(j=0; j<ysize; j++)
    {
        if(matrix[i-1][j]>target) break;
        if(matrix[i-1][j]==target) return true;
    }
    return false;
}
int main()
{
    vector<int> tmp;

    vector<vector<int> > matrix;
    tmp.push_back(1);
    tmp.push_back(3);
    matrix.push_back(tmp);

    cout<< searchMatrix(matrix, 3)<<endl;
    return 0;
}




