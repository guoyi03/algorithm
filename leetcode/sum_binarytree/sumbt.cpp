/*
 * Author     : Guo Yi
 * Create Time: 2013.10.07     
 * Problem    :Sum Root to Leaf Numbers
 *              Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.
 *              http://oj.leetcode.com/problems/sum-root-to-leaf-numbers/
 * Solution   :
 *
 */


#include<iostream>
#include <stack>
#include<cmath>
using namespace std;

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

stack<int> st; //store flag,  1 means we have scan node's right child
                            //0 means we have scan node's left child
                            //if 1, we should not scan this node ever

stack<TreeNode*> q;

//DFS
int sumNumbers(TreeNode *root) {
    int sumnum =0;
    int tmpsum = 0;
    TreeNode * cur ;
    TreeNode * tmp;

    if(root==NULL)return 0;

    cur = root;

    while(cur || !q.empty())
    {
        while(cur!=NULL)
        {
            tmpsum = tmpsum*10 + cur->val;

            q.push(cur);
            st.push(0);
            cur =  cur->left;
        }
        if(!q.empty())
        {
            cur = q.top();
            st.pop();
            st.push(1); //find right;

            tmp = cur;
            cur = cur->right;
            if(cur == NULL) 
            {
                if(tmp->left ==NULL && tmp->right ==NULL) //leaf node
                    sumnum = sumnum + tmpsum;
                while(!st.empty()&&st.top()==1)
                {
                    tmpsum = (tmpsum-tmp->val)/10;
                    st.pop();
                    q.pop();
                    if(!q.empty())
                        tmp = q.top();
                }
            }
        }
    }
    return sumnum;
    
}
int main()
{
    TreeNode *n1 = new TreeNode(1);
    TreeNode *n2 = new TreeNode(2);
    TreeNode *n3 = new TreeNode(3);
    TreeNode *n4 = new TreeNode(4);
    n1->left = n2;
    n1->right = n3;
    n2->right = n4;
    cout<<sumNumbers(n1)<<endl;

    delete n1,n2,n3,n4;
    return 0;
}




