/*
 * Author     : Guo Yi
 * Create Time: 2013.12.13
 * Problem    : Spiral Matrix
 *              Given a matrix of m x n elements (m rows, n columns)
 *              return all elements of the matrix in spiral order.
 *
 * Solution   : Traverse
 *
 */


#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;

//0: end
//1: right
//2: down
//3: left
//4: up
vector<vector<int> > tcopy;
int findnext(int x, int y, int flag)
{
    tcopy[x][y] = INT_MAX;
    if(flag==0) //up
    {
        if(x>0)
        {
            if(tcopy[x-1][y]!=INT_MAX)
                return 4;
        }
    }
right:
    if(y<tcopy[0].size()-1) 
    {
        if(tcopy[x][y+1]!=INT_MAX)
            return 1;
    }
down:
    if(x<tcopy.size()-1)
    {
        if(tcopy[x+1][y]!=INT_MAX)
            return 2;
    }
left:
    if(y>0)
    {
        if(tcopy[x][y-1]!=INT_MAX)
            return 3;
    }
    if(flag==0)
        goto end;
up:
    if(x>0)
    {
        if(tcopy[x-1][y]!=INT_MAX)
            return 4;
    }
end:
    return 0;
}
vector<int> spiralOrder(vector<vector<int> > &matrix) {
    vector<int>result;

    if(matrix.empty()) return result;
    int xsize= matrix.size();
    int ysize= matrix[0].size();
    int x,y;
    
    tcopy = matrix;
    
    int step = 1;
    int res=1;
    int last =0;
    x=0;
    y=0;
    result.push_back(matrix[0][0]);
    do
    {
        last = res;
        res = findnext(x,y,step%4);
        if(last!=res)
            step++;
        switch(res){
            case 1://right
                result.push_back(matrix[x][y+1]);
                y= y+1;
                break;
            case 2://down
                result.push_back(matrix[x+1][y]);
                x= x+1;
                break;
            case 3://left
                result.push_back(matrix[x][y-1]);
                y= y-1;
                break;
            case 4://up
                result.push_back(matrix[x-1][y]);
                x= x-1;
                break;
        }
    }while(res!=0);

    return result;
}
int main()
{
    vector<vector<int> > matrix;
    vector<int> v1;
    vector<int> v2;
    vector<int> v3;
    vector<int> v4;

    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    v1.push_back(4);

    v2.push_back(5);
    v2.push_back(6);
    v2.push_back(7);
    v2.push_back(8);

    v3.push_back(9);
    v3.push_back(10);
    v3.push_back(11);
    v3.push_back(12);

    v4.push_back(13);
    v4.push_back(14);
    v4.push_back(15);
    v4.push_back(16);
    matrix.push_back(v1);
    matrix.push_back(v2);
    matrix.push_back(v3);
    matrix.push_back(v4);

    vector<int> result;

    result = spiralOrder(matrix);

    int i;
    for(i=0;i<result.size();i++)
        cout<<result[i]<<" ";
    cout<<endl;
    return 0;
}




