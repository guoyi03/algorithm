/*
 * Author     : Guo Yi
 * Create Time: 2013.12.13
 * Problem    : Spiral Matrix II
 *              Given an integer n
 *              generate a square matrix filled with elements from 1 to n2 in spiral order.
 *
 * Solution   : Traverse
 *
 */


#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;

//0: end
//1: right
//2: down
//3: left
//4: up
vector<vector<int> > tcopy;
int findnext(int x, int y, int flag)
{
    tcopy[x][y] = INT_MAX;
    if(flag==0) //up
    {
        if(x>0)
        {
            if(tcopy[x-1][y]!=INT_MAX)
                return 4;
        }
    }
right:
    if(y<tcopy[0].size()-1) 
    {
        if(tcopy[x][y+1]!=INT_MAX)
            return 1;
    }
down:
    if(x<tcopy.size()-1)
    {
        if(tcopy[x+1][y]!=INT_MAX)
            return 2;
    }
left:
    if(y>0)
    {
        if(tcopy[x][y-1]!=INT_MAX)
            return 3;
    }
    if(flag==0)
        goto end;
up:
    if(x>0)
    {
        if(tcopy[x-1][y]!=INT_MAX)
            return 4;
    }
end:
    return 0;
}
vector<vector<int> > generateMatrix(int n) {
    vector<vector<int> >result;

    if(n==0) return result;
    
    int x,y;
    
    for(x=0;x<n;x++)
    {
        vector<int> tmp;
        for(y=0;y<n;y++)
            tmp.push_back(0);
        result.push_back(tmp);
        tcopy.push_back(tmp);
    }
    
    int step = 1;
    int res=1;
    int last =0;
    x=0;
    y=0;
    result[0][0] = 1;
    int num =1;
    int maxnum = n*n;
    do
    {
        num++;
        last = res;
        res = findnext(x,y,step%4);
        if(last!=res)
            step++;
        switch(res){
            case 1://right
                y= y+1;
                result[x][y] = num;
                break;
            case 2://down
                x= x+1;
                result[x][y] = num;
                break;
            case 3://left
                y= y-1;
                result[x][y] = num;
                break;
            case 4://up
                x= x-1;
                result[x][y] = num;
                break;
        }
    }while(num<=maxnum);

    return result;
}
int main()
{
    vector<vector<int> > result;


    result = generateMatrix(3);

    int i,j;
    for(i=0;i<result.size();i++)
    {
        for(j=0; j<result[0].size() ;j++)
            cout<<result[i][j]<<" ";
        cout<<endl;
    }
    return 0;
}




