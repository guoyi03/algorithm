/*
 * Author     : Guo Yi
 * Create Time: 2013.11.3
 * Problem    : Partition List 
 *              Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
 *              http://oj.leetcode.com/problems/partition-list/
 *
 * Solution   :
 *
 */


#include<iostream>

using namespace std;
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *partition(ListNode *head, int x) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    ListNode * reshead;
    ListNode * last;
    ListNode * iter;
    ListNode * curr;
    if(head==NULL) return head;

    if(head->next==NULL) return head;

    reshead = head; 
    last = head;
    iter = head->next;
    if(head->val >=x)
        curr = NULL;
    else
        curr = head;

    while(iter)
    {
        if(iter->val < x)
        {
            if(curr == NULL)
            {
                last->next = iter->next;
                iter->next = reshead; 
                reshead = iter;
            }
            else
            {
                if(curr!=last)
                {
                    last->next = iter->next;
                    iter->next = curr->next;
                    curr->next = iter;
                }
                else
                    last = iter;
            }
            curr = iter;
            iter = last->next;
        }
        else
        {
            last = iter;
            iter = iter->next;
        }
    }
    return reshead;
}
int main()
{
    ListNode* l1 = new ListNode(1);
    ListNode* l2 = new ListNode(4);
    ListNode* l3 = new ListNode(3);
    ListNode* l4 = new ListNode(2);
    ListNode* l5 = new ListNode(5);
    ListNode* l6 = new ListNode(2);
    
    l1->next = l2;
    l2->next = l3;
    l3->next = l4;
    l4->next = l5;
    l5->next = l6;
    
    ListNode*l0 = partition(l1,3);

    while(l0)
    {
        cout<< l0->val <<endl;
        l0 = l0->next;
    }
    // test {1,4} 5
    l1->next = l2;
    l2->next = NULL;
    l0 = partition(l1,5);

    while(l0)
    {
        cout<< l0->val <<endl;
        l0 = l0->next;
    }



    delete l1,l2,l3,l4,l5,l6;
    return 0;
}




