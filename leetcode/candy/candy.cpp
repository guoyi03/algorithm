/*
 * Author     : Guo Yi
 * Create Time: 2013.10.06
 * Problem    : Candy 
 *              There are N children standing in a line. 
 *              Each child is assigned a rating value.
 *              You are giving candies to these children subjected to the following requirements:
 *                   Each child must have at least one candy.
 *                   Children with a higher rating get more candies than their neighbors.
 *              What is the minimum candies you must give?
 *              http://oj.leetcode.com/problems/candy/
 * Solution   :
 *
 */


#include<iostream>
#include<vector>
using namespace std;
vector<int> tcopy;
vector<int> tcandy;

int findcandy(int i)
{
    int cur= tcopy[i];
    int num = 1;
    int candynum = 0;
    while((i+num)<tcopy.size() && cur>tcopy[i+num])
    {
        cur = tcopy[i+num];
        num++;
    }
    for(int j =0 ; j<num ;j++)
        tcandy[i+j] = num-j;
    return num;
}
int candy(vector<int> &ratings) {
    int mincandy= 0;
    int i =0;
    int size ;
    int step=0;
    int lastcandy=0;
    if(ratings.empty()) return 0;

    tcopy = ratings;
    tcandy = ratings;

    size =  ratings.size();

    while(i<size)
    {
        step= findcandy(i);
        if(step==1)
        {
            if((i-1>=0)&&(tcopy[i]==tcopy[i-1]))
                lastcandy = 1;
            else
                lastcandy = lastcandy +1;
            tcandy[i] = lastcandy;
        }
        else
        {
        //    mincandy= mincandy + step*(step+1)/2;
            if(i-1>=0)
                if(tcopy[i-1]<tcopy[i]&&tcandy[i-1]>=tcandy[i])
                    tcandy[i] = tcandy[i-1]+1;
            lastcandy = 1;
        }
        i = i +step;
    }
    for(i =0;i < size; i++)
    {
    //    cout<< tcandy[i];
        mincandy = mincandy + tcandy[i];
    }
    return mincandy;
}
int main()
{

    vector<int> v;
    v.push_back(6);
    v.push_back(2);
    v.push_back(2);
    v.push_back(2);
    v.push_back(2);
    cout<<candy(v)<<endl;
    return 0;
}




