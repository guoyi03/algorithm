/*
 * Author     : Guo Yi
 * Create Time: Unique Binary Search Trees 
 * Problem    : Given n, how many structurally unique BST's (binary search trees) that store values 1...n?
 *
 * Solution   : divide and conqure 
 *
 */


#include<iostream>
using namespace std;

int numTrees(int start, int end)
{
    if (start >= end)
        return 1;

    int totalNum = 0;
    for (int i=start; i<=end; ++i)
        totalNum += numTrees(start,i-1)*numTrees(i+1,end);
    return totalNum;
}

int numTrees(int n) 
{
    return numTrees(1,n);
}


int main()
{
    cout<<numTrees(3)<<endl;
    return 0;
}




