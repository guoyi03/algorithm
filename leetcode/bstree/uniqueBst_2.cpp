/*
 * Author     : Guo Yi
 * Create Time: Unique Binary Search Trees II 
 * Problem    : Given n, generate all structurally unique BST's (binary search trees) that store values 1...n.
 *              http://oj.leetcode.com/problems/unique-binary-search-trees-ii/
 * 
 * Solution   : divide and conqure 
 *
 */


#include<iostream>
#include<vector>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

vector<TreeNode *> Trees(int start, int end)
{
    vector<TreeNode *>  vtree;
    TreeNode * t1 = new TreeNode(start);
    if(start > end)
    {
        t1=NULL;
        vtree.push_back(t1);
        return vtree;
    }
    if(start ==end)
    {
        vtree.push_back(t1);
        return vtree;
    }
    for (int i=start; i<=end; ++i)
    {
        vector<TreeNode*> vtmp1 = Trees(start,i-1);
        vector<TreeNode*> vtmp2 = Trees(i+1,end);
        for(int j =0; j<vtmp1.size(); j++)
            for(int k =0; k<vtmp2.size(); k++)
            {    
                TreeNode * t2 = new TreeNode(i);
                t2->left = vtmp1[j];
                t2->right = vtmp2[k];
                vtree.push_back(t2);
            }
    }
    return vtree;
}


vector<TreeNode *> generateTrees(int n) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    if(n==0)
    {
         TreeNode*tmp =NULL;
         vector<TreeNode*> res;
         res.push_back(tmp);
        return res;
    }
    return Trees(1,n);
}

int main()
{
    vector<TreeNode*> res;
    res = generateTrees(3);
    cout<<res.size() <<endl;
    res.clear();
    return 0;
}




