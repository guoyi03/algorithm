/*
 * Author     : Guo Yi
 * Create Time: 2013.10.29
 * Problem    : Validate Binary Search Tree
 *              Given a binary tree, determine if it is a valid binary search tree (BST).
 *
 * Solution   : recursive
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
vector<TreeNode*> order;

void traverse(TreeNode* root)
{
    if(!root) return ;
    traverse(root->left);
    order.push_back(root);
    traverse(root->right);
}

bool isValidBST(TreeNode *root) {
    if(root==NULL)return true;
    order.clear();

    traverse(root);
    for(int i =0; i< order.size()-1; i++)
    {
        if(order[i]->val>=order[i+1]->val)
        {
            return false;
        }
    }
    return true;
}
int main()
{
    TreeNode* t1 = new TreeNode(2);
    TreeNode* t2 = new TreeNode(1);
    TreeNode* t3 = new TreeNode(3);

    t1->left = t2;
    t1->right = t3;

    cout<<isValidBST(t1)<<endl;
    return 0;
}




