/*
 * Author     : Guo Yi
 * Create Time: 2013.10.25
 * Problem    : Recover Binary Search Tree
 *              Two elements of a binary search tree (BST) are swapped by mistake.
 *              Recover the tree without changing its structure.
 *
 * Solution   : recursive
 */


#include<iostream>
#include<queue>
#include<vector>
#include<map>
#include<utility>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
vector<TreeNode*> order;

void traverse(TreeNode* root)
{
    if(!root) return ;
    traverse(root->left);
    order.push_back(root);
    traverse(root->right);
}
void recoverTree(TreeNode *root) 
{
    if(root==NULL)return;
    order.clear();

    traverse(root);

    TreeNode * first = NULL;
    TreeNode * second =NULL;
    int tmp;

    for(int i =0; i< order.size()-1; i++)
    {
        if(order[i]->val>order[i+1]->val && first==NULL)
        {
            first = order[i];
            if(i+1==order.size()-1)
                second = order[i+1];
            if((i+2<=order.size()-1)&&(order[i]->val < order[i+2]->val))
                second = order[i+1];
        }
        else
        {
            if(order[i]->val>order[i+1]->val && first!=NULL && second ==NULL)
                second = order[i+1];
        }
    }
    tmp = first->val;
    first->val = second->val;
    second->val = tmp;
    return;
}
int main()
{
    TreeNode* t1 = new TreeNode(2);
    TreeNode* t2 = new TreeNode(3);
    TreeNode* t3 = new TreeNode(1);

    t1->left = t2;
    t1->right = t3;

    recoverTree(t1);
    return 0;
}




