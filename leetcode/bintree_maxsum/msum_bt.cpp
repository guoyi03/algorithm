/*
 * Author     : Guo Yi
 * Create Time: 2013.10.14
 * Problem    : Binary Tree Maximum Path Sum
 *              Given a binary tree, find the maximum path sum.
 *              http://oj.leetcode.com/problems/binary-tree-maximum-path-sum/
 * Solution   : recursive algorithm + divide and conquer
 *              (should consider negative number)
 *              need to consider seven situation at most. 
 *
 */


#include<iostream>
#include <algorithm>
#include <vector>
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
/*
 * pathmax: the maximum path in binary tree
 * leafmax: the maximun path for root node
 */
int maxPLSum(TreeNode *root, int &pathmax, int &leafmax)
{
    int tpathmax = 0;
    int tleafmax = 0;
    pathmax = tpathmax;
    leafmax = tleafmax;
    if(root==NULL)
        return -1;  //don't compare

    int lpmax=0,rpmax=0;
    int llmax=0,rlmax=0;
    int res1=0, res2 =0;
    res1 = maxPLSum(root->left, lpmax, llmax);
    res2 = maxPLSum(root->right, rpmax, rlmax);
    if(res1==-1 && res2 ==-1)
    {
        leafmax = root->val;
        pathmax = root->val;
        return 0;
    }
    if(res1==-1) // no left child
    {
        leafmax = max(root->val, rlmax + root->val);
        pathmax = max(root->val,max(rpmax, rlmax + root->val));
        cout<<"current["<<root->val<<"] leafmax: " << leafmax << " pathmax: "<<pathmax <<endl;
        return 0;
    }
    if(res2==-1)
    {
        leafmax = max(root->val,llmax + root->val);
        pathmax = max(root->val, max(lpmax, llmax + root->val));
        cout<<"current["<<root->val<<"] leafmax: " << leafmax << " pathmax: "<<pathmax <<endl;
        return 0;
    }

    leafmax = max(root->val,max( llmax+root->val , rlmax + root->val ));

    vector<int> temp;
    temp.push_back(lpmax);
    temp.push_back(rpmax);
    temp.push_back(llmax);
    temp.push_back(rlmax);
    temp.push_back(llmax + root->val);
    temp.push_back(rlmax + root->val);
    temp.push_back(llmax + rlmax + root->val);
    temp.push_back(root->val);
    pathmax = *max_element(temp.begin(),temp.end());
    cout<<"current["<<root->val<<"] leafmax: " << leafmax << " pathmax: "<<pathmax <<endl;
    return 0;
}

int maxPathSum(TreeNode *root) {

    int pathmax =0;
    int leafmax = 0;
    if(root == NULL) return 0;
        
    maxPLSum(root,pathmax,leafmax);
    return pathmax;
}


int main()
{
#if 0
    TreeNode* node0 = new TreeNode(1);
    TreeNode* node1 = new TreeNode(-2);
    TreeNode* node2 = new TreeNode(-3);
    TreeNode* node3 = new TreeNode(1);
    TreeNode* node4 = new TreeNode(3);
    TreeNode* node5 = new TreeNode(-2);
    TreeNode* node6 = new TreeNode(-1);

    node0->left = node1;
    node0->right = node2;
    node1->left = node3;
    node1->right = node4;
    node2->left = node5;
    node3->right = node6;
#endif
#if 1 
    TreeNode* node0 = new TreeNode(-1);
    TreeNode* node1 = new TreeNode(5);
    TreeNode* node2 = new TreeNode(4);
    TreeNode* node3 = new TreeNode(2);
    TreeNode* node4 = new TreeNode(-4);

    node0->left = node1;
    node1->left = node2;
    node2->right = node3;
    node3->left = node4;
#endif
    cout<<maxPathSum(node0)<<endl;
    
    return 0;
}




