/*
 * Author     : Guo Yi
 * Create Time: 2013.11.21
 * Problem    : Climbing Stairs
 *              You are climbing a stair case. It takes n steps to reach to the top.
 *              http://oj.leetcode.com/problems/climbing-stairs/
 * Solution   : Fibonacci
 *
 */


#include<iostream>
using namespace std;

int climbStairs(int n) {
    if(n==0) return 0;
    if(n==1) return 1;
    int*result = new int[n];
    result[0] = 1;
    result[1] = 2;
    int i =0;
    for(i=2; i<n ;i++)
        result[i]=result[i-1]+result[i-2];
    int res = result[n-1];
    delete result;
    return res;
}
int main()
{
    cout<<climbStairs(5)<<endl;
    return 0;
}




