/*
 * Author     : Guo Yi
 * Create Time: 2013.11.1
 * Problem    : Scramble String
 *              Given two strings s1 and s2 of the same length, determine if s2 is a scrambled string of s1.
 *              http://oj.leetcode.com/problems/scramble-string/
 *
 * Solution   : divide and conquer
 *
 */


#include<iostream>
#include<string>
#include<map>
using namespace std;

bool hasSame(string s1, string s2)
{
    map<char,int> hmap1;
    map<char,int> hmap2;
    int i;
    string str ="";
    hmap1.clear();
    hmap2.clear();
    for(int i =0; i<s1.size(); i++)
    {
        if(str.find(s1[i])==string::npos)str = str + s1[i];
        hmap1[s1[i]] ++;
        hmap2[s2[i]] ++;
    }
    for(i =0 ;i< str.size() ; i++)
    {
        if(hmap1[str[i]]!=hmap2[str[i]])return false;
    }
    return true;
}

bool isScramble(string s1, string s2) {
    // IMPORTANT: Please reset any member data you declared, as
    // the same Solution instance will be reused for each test case.
    if(s1.empty()||s2.empty()) return false;
    if(s1.size()!=s2.size()) return false;

    if(s1==s2) return true;
    if(s1.size()==1) 
        return (s1==s2);

    int i =0;
    int size = s1.size();
    if(!hasSame(s1,s2)) return false;
    for(i =0 ; i < size-1; i++)
    {
      //  if(hasSame(s1.substr(0,i+1),s2.substr(0,i+1)))
        {
            if(isScramble(s1.substr(0,i+1),s2.substr(0,i+1))&&
                isScramble(s1.substr(i+1),s2.substr(i+1)))
                return true;
        }
      //  if(hasSame(s1.substr(0,i+1),s2.substr(size-1-i,i+1)))
        {
            if(isScramble(s1.substr(0,i+1),s2.substr(size-1-i))&&
                    isScramble(s1.substr(i+1),s2.substr(0,size-i-1)))
                return true;
        }
    }
    return false;
}
int main()
{
    string s1 ="hobobyrqd";
    string s2 ="hbyorqdbo";

    cout<< isScramble(s1, s2) <<endl;
    return 0;
}




