/*
 * Author     : Guo Yi
 * Create Time: 2013.10.15
 * Problem    : Best Time to Buy and Sell Stock
 *              Say you have an array for which the ith element is the price of a given stock on day i.
 *              design an algorithm to find the maximum profit.
 *              http://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock/
 * Solution   : Search from end to start, record maxprofit and top
 *
 */


#include<iostream>
#include<vector>
using namespace std;

int maxProfit(vector<int> &prices) {
    
    int maxpro =0;
    int top =0;
    int size = prices.size();
    if(size <=1) return 0;

    top = prices[size-1];

    for(int i = size-2;  i>=0 ;i--)
    {
        if(top - prices[i] > maxpro) 
            maxpro = top - prices[i];
        top = max(prices[i],top);
    }
    return maxpro;
    
}
int main()
{
    vector<int> prices;
    prices.push_back(100);
    prices.push_back(10);
    prices.push_back(100);
    cout << maxProfit(prices) <<endl;

    return 0;
}




