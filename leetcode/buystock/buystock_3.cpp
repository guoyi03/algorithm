/*
 * Author     : Guo Yi
 * Create Time: 2013.10.16
 * Problem    : Best Time to Buy and Sell Stock III
 *              Say you have an array for which the ith element is the price of a given stock on day i.
 *              design an algorithm to find the maximum profit.(can complete as most two transactions)
 *              http://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/
 * Solution   : 
 *              Solution 1:
 *                  step dynamicly.
 *              Solution 2:
 *                  first: search from end to start to calculate Maxprofit[i,n-1]
 *                  second:search from start to end to calculate Maxprofit[0,i]
 *                  then can find the maxprofit that complete as most tow transactions
 *
 */


#include<iostream>
#include<vector>
using namespace std;

int maxProfitOne(vector<int> &prices, int start, int end, int &up, int &down) {
    
    int maxpro =0;
    int top = 0;
    down = 0;
    up =0;
    int size = end - start;
    if(size <=1) 
    {
        up = prices[start];
        down = prices[start];
        return 0;
    }

    top = prices[end-1];
    up = end-1;
    for(int i = end-1;  i>=start ;i--)
    {
            if(top - prices[i] > maxpro) 
            {
                maxpro = top - prices[i];
                down = i;
            }
            if(top<prices[i]) up = i;
            top = max(prices[i],top);
    }

    return maxpro;
    
}

int maxProfit(vector<int> &prices) {
    
    int maxpro =0;
    int up1,down1;
    int up2,down2;

    int max1 = 0;
    int max2 =0;
    int size = prices.size();
    if(size <=1) return 0;
    
    int i =1;

    max2 = maxProfitOne(prices,0,size, up2, down2);
    i = down2;
    while(i<size-1)
    {
        max1 = maxProfitOne(prices,0,i, up1,down1);
        maxpro = max(maxpro, max1+max2);
        max2 = maxProfitOne(prices,i+1,size,up2,down2);
        if(max2==0)break;
        i = down2;
    }
    return maxpro;
    
}
int main()
{
    vector<int> prices;
    prices.push_back(1);
    prices.push_back(4);
    prices.push_back(2);
    cout << maxProfit(prices) <<endl;

    return 0;
}




