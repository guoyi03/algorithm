/*
 * Author     : Guo Yi
 * Create Time: 2013.10.16
 * Problem    : Best Time to Buy and Sell Stock
 *              Say you have an array for which the ith element is the price of a given stock on day i.
 *              design an algorithm to find the maximum profit.(can complete as many transactions as you like)
 *              http://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
 * Solution   : Search from start to end, transact as much as posibile.(if there has a chance to get profit)
 *
 */


#include<iostream>
#include<vector>
using namespace std;

int maxProfit(vector<int> &prices) {
    
    int maxpro =0;
    int start = 0;
    int end =0;
    int size = prices.size();
    if(size <=1) return 0;

    while(end < size-1)
    {
        while((end<size-1)&&(prices[end+1] >= prices[end]))
            end ++;
        
        maxpro = prices[end] - prices[start] + maxpro;
        start = end + 1;
        while((start<size-1) &&(prices[start+1] <= prices[start]))
            start++;
        end = start;
    }
    return maxpro;
    
}
int main()
{
    vector<int> prices;
    prices.push_back(10);
    prices.push_back(100);
    prices.push_back(10);
    prices.push_back(100);
    cout << maxProfit(prices) <<endl;

    return 0;
}




