/*
 * Author     : Guo Yi
 * Create Time: 2013.11.30
 * Problem    : Sum of numbers
 *              Given an array of N elements, check if it is possible to obtain a sum of S
 *              by choosing some (or none) elements of the array and adding them.
 *              http://www.hackerearth.com/problem/algorithm/sum-of-numbers-9/
 *
 * Solution   : recursive 
 *
 */

#include<iostream>
#include<stdio.h>
using namespace std;

//[start,end]

bool subsum(int a[], int start, int end, int sum ){
    if(sum==0) return true;

    if(end >=0)
        return subsum(a, start, end-1, sum - a[end]) || subsum(a,start,end-1,sum);

    return false;
}
int main()
{
    int tmp;
    int sum;
    int num;
    int i=0;
    int j=0;
    scanf("%d",&tmp); // number of test cases
    for(i =0; i< tmp; i++)
    {
        scanf("%d", &num);
        int a[16];
        for(j=0; j< num ;j++)
            scanf("%d",a+j);
        scanf("%d", &sum);
        if(subsum(a,0,num-1,sum))
            printf("YES\n");
        else
            printf("NO\n");
    }
    return 0;
}




