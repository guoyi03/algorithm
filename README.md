Writting Algorithms 
======================

### Personal user guide

A new process about using git in algorithm project. (everyone build a new branch to solve a new problem locally, then merge to master,push to server)

##### 1.Start
       
        $git clone https://guoyi03@bitbucket.org/guoyi03/algorithm.git
        $git checkout -b problemA      //try to solve problem A
        $<edit.......>                 //program, compile, test
        $git commit -a                 //commit to local branch problemA
        
        $git checkout master           //change to master (local)
        $git pull                      //download the latest updates and move to latest version
        $git merge --squash problemA   //merge  problemA to master(local) 
        $git commit -a                 //commit to local branch
        $git push origin master        //push to server(url)
        $git branch -d problemA        //delete branch problemA (local)


